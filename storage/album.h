#pragma once
#include <iostream>

#include <cstring>
#include <string>

using namespace std;

struct Album
{
    int id;
    string fullname;
    string kind;
};
