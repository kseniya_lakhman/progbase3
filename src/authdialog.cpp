#include "authdialog.h"
#include "registerdialog.h"
#include "ui_authdialog.h"
#include <QCryptographicHash>
#include <QDebug>
#include <QMessageBox>

AuthDialog::AuthDialog(User *user, Storage *storage, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AuthDialog)
{
    ui->setupUi(this);
    stor = storage;
    change = user;
    ui->userAuth_label->setText("<b> Enter your data to log in: <b/>");
}

AuthDialog::~AuthDialog()
{
    delete ui;
}

void AuthDialog::on_buttonBox_accepted()
{
    change->username = ui->username_lineEdit->text().toStdString();
    string entered_password = ui->password_lineEdit->text().toStdString();
    QString hash = QString(QCryptographicHash::hash((entered_password.c_str()),QCryptographicHash::Md5).toHex());
    change->password_hash = hash.toStdString();
}

void AuthDialog::on_pushButton_clicked()
{
    User user;
    user.id = -1;
    if (this->stor == nullptr)
    {
        QMessageBox::information(
            this,
            "Sign in",
            "Please, open the storage!");
        return;
    }

    qDebug() << "Registration";

    RegisterDialog registerDialog(&user, this->stor);
    registerDialog.setWindowTitle("Sign in");
    int status = registerDialog.exec();

    if (status == 1)
    {
          qDebug() << "status == 1" << user.username.c_str() << " - " << user.password_hash.c_str();
          if(this->stor->registrateUserData(user.username, user.password_hash) != nullopt)
          {
              QMessageBox::information(
                  this,
                  "Sign in",
                  "Registrarion successfull!");
              return;
          }
          else
          {
              QMessageBox::warning(
                  this,
                  "Sigh in",
                  "Signing!!!! up error: enter the correct original data");
          }
     }
    else
    {
              qDebug() << "Rejected!";
    }

}
