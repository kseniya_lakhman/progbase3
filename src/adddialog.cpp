#include "adddialog.h"
#include "ui_adddialog.h"
#include <QWidget>
#include <QDebug>
#include <QSpinBox>
#include <QFileDialog>
#include <QMessageBox>

Q_DECLARE_METATYPE(Composer*)

AddDialog::AddDialog(Composer *composer, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AddDialog)
{
    ui->setupUi(this);
    change = composer;
    ui->newComposer_label->setText("<b> Add new composer: <b/>");

    qDebug() << "Data entered";
}

AddDialog::~AddDialog()
{
    delete ui;
}

void AddDialog::on_buttonBox_accepted()
{
        change->fullname = ui->fullname_lineEdit->text().toStdString();
        change->year = ui->year_spinBox->text().toInt();
        change->amount = ui->amount_spinBox->text().toInt();
}

void AddDialog::on_chooseImage_Button_clicked()
{
        QString fileName = QFileDialog::getOpenFileName(
                    this,              // parent
                    "Choose",  // caption
                    "../../src/data/images",                // directory to start with
                    "Images (*.png *.jpg *.jpeg *.gif);;All Files (*)");  // file name filter
        qDebug() << fileName;
        if (fileName != nullptr)
        {
            QImage image;
            bool valid = image.load(fileName);
            if(valid)
            {
                image = image.scaledToWidth(ui->image_label->width(), Qt::SmoothTransformation);
                image = image.scaledToHeight(ui->image_label->height(), Qt::SmoothTransformation);
                ui->image_label->setPixmap(QPixmap::fromImage(image));
                this->filename = fileName;
            }else
            {
                qDebug() << "Error";
            }
        }
}
