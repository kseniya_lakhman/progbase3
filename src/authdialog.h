#ifndef AUTHDIALOG_H
#define AUTHDIALOG_H

#include <QDialog>
#include "storage.h"

namespace Ui {
class AuthDialog;
}

class AuthDialog : public QDialog
{
    Q_OBJECT

public:
    explicit AuthDialog(User *user, Storage *storage, QWidget *parent = 0);
    ~AuthDialog();

private slots:
    void on_buttonBox_accepted();

    void on_pushButton_clicked();

private:
    Ui::AuthDialog *ui;
    User * change;
    Storage *stor;
};

#endif // AUTHDIALOG_H
