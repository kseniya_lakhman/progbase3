#include "sqlite_storage.h"
#include <QtSql>
#include <QMessageBox>
#include <xml_storage.h>

SqliteStorage::SqliteStorage(const string &dir_name) : dir_name_(dir_name)
{
    db_ = QSqlDatabase::addDatabase("QSQLITE");
}

bool SqliteStorage::open()
{
    QString path = QString::fromStdString(this->dir_name_);
    db_.setDatabaseName(path);    // set sqlite database file path
    bool connected = db_.open();  // open db connection
    if (!connected) {
      db_.close();  // close db connection
      return false;
    }
    return true;
}
bool SqliteStorage::close()
{
    db_.close();
    return true;
}

Composer getComposerFromQuery(const QSqlQuery & query)
{
    int id = query.value("id").toInt();
    string fullname = query.value("fullname").toString().toStdString();
    int year = query.value("year").toInt();
    int amount = query.value("amount").toInt();

    Composer c;
    c.id = id;
    c.fullname = fullname;
    c.year = year;
    c.amount = amount;
    return c;
}
// composers
vector<Composer> SqliteStorage::getAllUserComposers(int user_id)
{
    vector<Composer> composers;
    qDebug() << user_id << "us id";

    QSqlQuery query;
    query.prepare("SELECT * FROM composers WHERE user_id = :id");
    query.bindValue(":id", user_id);

    if (!query.exec())
        {
           qDebug() << "get composer error:" << query.lastError();
           return composers;
        }
    else
    {
        while (query.next())
        {
            Composer c = getComposerFromQuery(query);
            composers.push_back(c);
        }
    }

    return composers;
}
vector<Composer> SqliteStorage::getAllComposers(void)
{
    vector<Composer> composers;
    QSqlQuery query("SELECT * FROM composers");
    while (query.next())
    {
        Composer c = getComposerFromQuery(query);
        composers.push_back(c);
    }
    return composers;

}
optional<Composer> SqliteStorage::getComposerById(int composer_id)
{
    QSqlQuery query;
    query.prepare("SELECT * FROM composers WHERE id = :id");
    query.bindValue(":id", composer_id);
    if (!query.exec())
        {
        // do exec if query is prepared SELECT query
           qDebug() << "get composer error:" << query.lastError();
            return nullopt;
        }

    if (query.next()) {
        Composer c = getComposerFromQuery(query);
        return c;
    }

    return nullopt;
}

bool SqliteStorage::updateComposer(const Composer &composer)
{
    QSqlQuery query;
    query.prepare("UPDATE composers SET fullname = :fullname, year= :year, amount = :amount WHERE id = :id"); //composition = :composition,

    query.bindValue(":fullname", QString::fromStdString(composer.fullname));
    query.bindValue(":year", composer.year);
    query.bindValue(":amount", composer.amount);
    query.bindValue(":id", composer.id);
    if (!query.exec()){
        qDebug() << "updateComposer error:" << query.lastError();
        return false;
    }
    return true;
}

bool SqliteStorage::removeComposer(int composer_id)
{
    //remove all composer albums
    vector <Album> composerAlbums = this->getAllComposerAlbums(composer_id);
    if (composerAlbums.size() != 0)
    {
        foreach (Album al, composerAlbums)
        {
            this->removeComposerAlbum(composer_id, al.id);
        }
    }

    //remove composer image
    QString composerImage = this->getComposerImage(composer_id);
    if(!composerImage.isEmpty())
    {
        this->removeImage(composer_id);
    }

    //remove composer
    QSqlQuery query;
    query.prepare("DELETE FROM composers WHERE id = :id AND user_id = :us_id");
    query.bindValue(":id", composer_id);
    query.bindValue(":us_id", this->userId);

    if (!query.exec()){
        qDebug() << "deleteComposer error:" << query.lastError();
        return false;
    }
    if (query.numRowsAffected() == 0)
    {
        return false;
    }
    return true;
}
int SqliteStorage::insertComposer(const Composer & composer)
{
    if(isComposerExist(composer))
    {
        qDebug() << "Such composer has already been in user " << this->userId << " database";
        return -1;

    }

    QSqlQuery query;
    query.prepare("INSERT INTO composers (fullname, year, amount, user_id)"
                  "VALUES (:fullname, :year, :amount, :user_id)"); //composition
    query.bindValue(":fullname", QString::fromStdString(composer.fullname));
    query.bindValue(":year", composer.year);

    query.bindValue(":amount", composer.amount);
    query.bindValue(":user_id", this->userId);
    if (!query.exec()){
//        bool success = query.next();
        qDebug() << "addPerson error:"
                 << query.lastError();
        return 0;
    }
    QVariant var = query.lastInsertId();
     qDebug() << "id : " << var.toString();
    return var.toInt();
}
int SqliteStorage::getNumberOfUserComposers(int user_id)
{
    if (this->userId == -1 || user_id == -1)
    {
        return 0;
    }

    QSqlQuery query;
    query.prepare("SELECT * FROM composers WHERE user_id = :id");
    query.bindValue(":id", user_id);

    int counter = 0;
    if (!query.exec())
        {
           qDebug() << "get composer error:" << query.lastError();
        }
    else
    {
        while (query.next())
        {
            counter += 1;
        }
    }

    return counter;
}
int SqliteStorage::getNumberOfSearchedUserComposers(int user_id, string & search_text)
{
    if (user_id == -1)
    {
        qDebug() << "Error: userId == -1";
        return 0;
    }

    QSqlQuery query;
    query.prepare("SELECT * FROM composers WHERE user_id = :id AND fullname LIKE ('%' || :search_text || '%')" );

    query.bindValue(":id", this->userId);
    query.bindValue(":search_text", QString::fromStdString(search_text));

    int counter = 0;
    if (!query.exec())
        {
           qDebug() << "get composer error:" << query.lastError();
        }
    else
    {
        while (query.next())
        {
            counter += 1;
        }
    }

    return counter;
}
// albums
Album getAlbumFromQuery(const QSqlQuery & query)
{
    int id = query.value("id").toInt();
    string fullname = query.value("fullname").toString().toStdString();
    string kind = query.value("kind").toString().toStdString();
    Album a;
    a.id = id;
    a.fullname = fullname;
    a.kind = kind;
    return a;
}
vector<Album> SqliteStorage::getAllAlbums(void)
{
    vector<Album> albums;
    QSqlQuery query("SELECT * FROM albums");
    while (query.next())
    {
        Album a = getAlbumFromQuery(query);
        albums.push_back(a);
    }
    return albums;
}
optional<Album> SqliteStorage::getAlbumById(int album_id)
{
    QSqlQuery query;
    query.prepare("SELECT * FROM albums WHERE id = :id");
    query.bindValue(":id", album_id);
    if (!query.exec()) {  // do exec if query is prepared SELECT query
       qDebug() << "get album error:" << query.lastError();
        return nullopt;
    }
    if (query.next()) {
        Album a = getAlbumFromQuery(query);
        return a;
    }
    return nullopt;
}
bool SqliteStorage::updateAlbum(const Album &album)
{
    QSqlQuery query;
    query.prepare("UPDATE albums SET fullname = :fullname, kind = :kind WHERE id = :id");

    query.bindValue(":fullname", QString::fromStdString(album.fullname));
    query.bindValue(":kind", QString::fromStdString(album.kind));
    query.bindValue(":id", album.id);

    if (!query.exec()){
        qDebug() << "updateAlbum error:" << query.lastError();
        return false;
    }
    return true;
}
bool SqliteStorage::removeAlbum(int album_id)
{
    QSqlQuery query;
    query.prepare("DELETE FROM albums WHERE id = :id");
    query.bindValue(":id", album_id);
    if (!query.exec()){
        qDebug() << "deleteAlbum error:" << query.lastError();
        return false;
    }
    if (query.numRowsAffected() == 0)
    {
        return false;
    }
    return true;
}
int SqliteStorage::insertAlbum(const Album & album)
{
    QSqlQuery query;
    query.prepare("INSERT INTO albums (fullname, kind)"
                  "VALUES (:fullname, :kind)");

    query.bindValue(":fullname", QString::fromStdString(album.fullname));
    query.bindValue(":kind", QString::fromStdString(album.kind));

    if (!query.exec()){
        qDebug() << "addAlbum error:" << query.lastError();
        return 0;
    }
    QVariant var = query.lastInsertId();
     qDebug() << "id : " << var.toString();
    return var.toInt();
}

bool foundUserInQuery(string & username)
{
    QSqlQuery query;
    query.prepare("SELECT * FROM users");

    if (!query.exec())
    {
        qDebug() << "database error: " << query.lastError();
        return false;
    }

    while (query.next())
       {
           string q_username = query.value("username").toString().toStdString();

           if (username == q_username)
           {
              return true;
           }

       }
    return false;
}

//users
optional<User> SqliteStorage::getUserAuth(string & username, string & password)//passwordHASH
{
    QSqlQuery query;
    query.prepare("SELECT * FROM users");
    if (!query.exec())
    {
        qDebug() << "database error: " << query.lastError();
        return nullopt;
    }

    while (query.next())
       {
           int q_id = query.value("id").toInt();
           string q_username = query.value("username").toString().toStdString();
           string q_passwordHash = query.value("password_hash").toString().toStdString();

           if (username == q_username)
           {
               qDebug() << "Found in database. Check the password...";
               if (password == q_passwordHash)
               {
                   qDebug() << "Successfull!";
                   User us;
                   us.id = q_id;
                   us.username = username;
                   us.password_hash = password;
                   this->userId = q_id;
                   qDebug() << this->userId << "userId !!!";
                   return us;
               }
           }

       }

    qDebug() << "nullopt";
    return nullopt;

}
optional<User> SqliteStorage::registrateUserData(string & username, string & password)//passwordHASH
{
    qDebug() << "hi!";
    if (foundUserInQuery(username) == false && username.size() != 0 && password.size() != 0)
    {
        qDebug() << "NOt found in database";

        QSqlQuery query;
        query.prepare("INSERT INTO users (username, password_hash)"
                      "VALUES (:userName, :passwordHash)");
        query.bindValue(":userName", QString::fromStdString(username));
        query.bindValue(":passwordHash", QString::fromStdString(password));

        if (!query.exec())
        {
            qDebug() << "users database error: " << query.lastError();
            return nullopt;
        }

        QVariant var = query.lastInsertId();
        qDebug() << "new user" << username.c_str() << "id : " << var.toString();

        User user;
        user.id = var.toInt();
        user.username = username;
        user.password_hash = password;

        return user;
    }
    else
    {
        qDebug() << username.c_str() << "has already in database";
    }
    qDebug() << "nullopt";
    return nullopt;
}
// links
vector<Album> SqliteStorage::getAllComposerAlbums(int composer_id)
{
    QSqlQuery query;
    query.prepare("SELECT * FROM links WHERE comp_id = :composer_id");
    query.bindValue(":composer_id", composer_id);

    if (!query.exec())
    {
        qDebug() << "get composer_id error:" << query.lastError();
    }
    vector<Album> albums;
    while (query.next())
    {
        int album_id = query.value("album_id").toInt();
        Album al = *getAlbumById(album_id);
        albums.push_back(al);
    }
    if (albums.size() == 0)
    {
        qDebug() << "size ==== 0 ";
    }
    return albums;
}

bool SqliteStorage::insertComposerAlbum(int composer_id, int album_id)
{
    QSqlQuery query;
    query.prepare("INSERT INTO links (comp_id, album_id)"
                  "VALUES (:comp_id, :album_id)");
    query.bindValue(":comp_id", composer_id);
    query.bindValue(":album_id", album_id);

    if (!query.exec()){
        qDebug() << "Linking album with composer error:" << query.lastError();
        return false;
    }
    QVariant var = query.lastInsertId();
     qDebug() << "id : " << var.toString();
    return true;
}
bool SqliteStorage::removeComposerAlbum(int composer_id, int album_id)
{
    QSqlQuery query;
    query.prepare("DELETE FROM links WHERE comp_id = :compID AND album_id = :albumID");
    query.bindValue(":compID", composer_id);
    query.bindValue(":albumID", album_id);

     qDebug() << "del...";
    if (!query.exec()){
        qDebug() << "deleteAlbum error:" << query.lastError();
        return false;
    }
    if (query.numRowsAffected() == 0)
    {
        return false;
    }
    return true;
}

//images

bool SqliteStorage::insertImage(int composer_id, string & filename)
{
    QSqlQuery query;
    query.prepare("INSERT INTO images (comp_id, fileway)"
                  "VALUES (:comp_id, :fileway)");
    query.bindValue(":comp_id", composer_id);
    query.bindValue(":fileway", QString::fromStdString(filename));

    if (!query.exec()){
        qDebug() << "Images table inserting error:" << query.lastError();
        return false;
    }
    return true;

    return false;
}
bool SqliteStorage::removeImage(int composer_id)
{
      QSqlQuery query;
      query.prepare("DELETE FROM images WHERE comp_id = :compID");
      query.bindValue(":compID", composer_id);

      if (!query.exec()){
          qDebug() << "Image deleting error:" << query.lastError();
          return false;
      }
      if (query.numRowsAffected() == 0)
      {
          return false;
      }
      return true;
}
QString SqliteStorage::getComposerImage(int composer_id)
{
    QSqlQuery query;
    query.prepare("SELECT * FROM images");

    if (!query.exec())
    {
       qDebug() << "get image filename error:" << query.lastError();
        return nullptr;
    }
    while (query.next())
    {
        int id = query.value("comp_id").toInt();
        if (id == composer_id)
        {
            string filename = query.value("fileway").toString().toStdString();
            if(filename.size() != 0)
            {
                QString fileway = QString::fromStdString(filename);
                return fileway;
            }else
            {
                qDebug() << "no image for composer with id " << composer_id;
                return nullptr;
            }
        }
    }
    return nullptr;
}
bool SqliteStorage::updateComposerImage(int composer_id, string & filename)
 {
    if(filename.size() > 1)
    {
        QSqlQuery query;
        query.prepare("UPDATE images SET fileway = :fileWay WHERE comp_id = :id");
        qDebug() << "NEW FILE NAME" << filename.c_str();
        query.bindValue(":fileWay", QString::fromStdString(filename));
        query.bindValue(":id", composer_id);

        if (!query.exec()){
            qDebug() << "updateImage error:" << query.lastError();
            return false;
        }
        return true;
    }
    return false;
 }

//pagination
vector<Composer> SqliteStorage::getSomeUserComposers(int user_id, int page_size, int page_number)
{
        vector<Composer> composers;

        QSqlQuery query;
        int skipped_items = (page_number - 1) * page_size;
        query.prepare("SELECT * FROM composers WHERE user_id = :id LIMIT :page_size OFFSET :skipped_items");

        query.bindValue(":id", user_id);
        query.bindValue(":page_size", page_size);
        query.bindValue(":skipped_items", skipped_items);

        if (!query.exec())
            {
               qDebug() << "get composer error:" << query.lastError();
               return {};
            }
        else
        {
            while (query.next())
            {
                Composer c = getComposerFromQuery(query);
                composers.push_back(c);
            }
        }
        return composers;
}
vector<Composer> SqliteStorage::getSeachedUserComposers( int user_id, int page_size, int page_number, string & search_text)
{
    vector<Composer>  composers;
    int skipped_items = (page_number - 1) * page_size;

    QSqlQuery query;
    query.prepare("SELECT * FROM composers WHERE user_id = :id AND fullname LIKE ('%' || :search_text || '%') LIMIT :page_size OFFSET :skipped_items" );

    query.bindValue(":id", this->userId);
    query.bindValue(":search_text", QString::fromStdString(search_text));
    query.bindValue(":page_size", page_size);
    query.bindValue(":skipped_items", skipped_items);

    if (!query.exec())
        {
           qDebug() << "get searching composer error:" << query.lastError();
           return {};
        }
    else
    {
        while (query.next())
        {
            Composer c = getComposerFromQuery(query);
            composers.push_back(c);
        }
    }
    return composers;

}

bool SqliteStorage::isComposerExist(Composer c)
{
    QSqlQuery query;
    query.prepare("SELECT COUNT(*) FROM composers WHERE fullname = :fullName AND user_id =:us_id");
    query.bindValue(":fullName", QString::fromStdString(c.fullname));
    query.bindValue(":us_id", this->userId);

    if (!query.exec()){
        qDebug() << "Error: counting rows with composer fullname:" << query.lastError();
        return false;
    }

    if (query.next())
    {
        int rows = query.value(0).toInt();
        if (rows == 0)
        {
            return false;
        }
        else if (rows >= 1)
        {
            qDebug() << "Finding rows: " << rows << c.fullname.c_str();
            return true;
        }
    }
    return true;
}

bool SqliteStorage::isAlbumExist(Album a)
{
    QSqlQuery query;
    query.prepare("SELECT COUNT(*) FROM albums WHERE fullname = :fullName");
    query.bindValue(":fullName", QString::fromStdString(a.fullname));

    if (!query.exec()){
        qDebug() << "Error: counting rows with composer fullname:" << query.lastError();
        return false;
    }

    if (query.next())
    {
        int rows = query.value(0).toInt();
        if (rows == 0)
        {
            return false;
        }
        else if (rows >= 1)
        {
            qDebug() << "Finding rows: " << rows << a.fullname.c_str();
            return true;
        }
    }
    return true;
}

int SqliteStorage::getComposerIndex(Composer c)
{
    int index = -1;

    QSqlQuery query;
    query.prepare("SELECT id FROM composers WHERE fullname = :fullName AND user_id =:us_id");
    query.bindValue(":fullName", QString::fromStdString(c.fullname));
    query.bindValue(":us_id", this->userId);

    if (!query.exec()){
        qDebug() << "Error: getting composer index fullname:" << query.lastError();
        return index;
    }

    if (query.next())
    {
        index = query.value(0).toInt();
    }
    return index;
}
int SqliteStorage::getAlbumIndex(Album a)
{
    int index = -1;

    QSqlQuery query;
    query.prepare("SELECT id FROM albums WHERE fullname = :fullName");
    query.bindValue(":fullName", QString::fromStdString(a.fullname));

    if (!query.exec()){
        qDebug() << "Error: getting album index fullname:" << query.lastError();
        return index;
    }

    if (query.next())
    {
        index = query.value(0).toInt();
    }
    return index;
}
bool isFullComposerData(Composer & c)
{
    if(c.fullname.size() != 0 &&
            c.amount >= 0 &&

            c.id != 0 &&
            c.year >= 1)
    {
        return true;
    }
    return false;
}
bool isFullAlbumData(Album & a)
{
    if(a.fullname.size() != 0 &&
            a.kind.size() != 0)
    {
        return true;
    }
    return false;
}

//import/export
void SqliteStorage::importComposersFromXML(string & filename)
{
    XmlStorage XmlStorageFile;

    vector <Composer> composers = XmlStorageFile.getAllComposersFromXML(filename);
    foreach(Composer c, composers)
    {
        bool isComposerInTable = isComposerExist(c);
        bool isFullData = isFullComposerData(c);

        if (isFullData && !isComposerInTable)
        {
            this->insertComposer(c);
        }
        else if (isFullData && isComposerInTable)
        {
            c.id = getComposerIndex(c);
            this->updateComposer(c);
        }else
        {
            qDebug() << "Error: composer data is not full. The element with fullname will not be added";
        }
    }
}
void SqliteStorage::importAlbumsFromXML(string & filename)
{
    XmlStorage XmlStorageFile;

    vector <Album> albums = XmlStorageFile.getAllAlbumsFromXML(filename);
    foreach(Album a, albums)
    {
        bool isAlbumInTable = isAlbumExist(a);
        bool isFullData = isFullAlbumData(a);

        if (isFullData && !isAlbumInTable)
        {
            qDebug() << "Album inserting...";
            this->insertAlbum(a);
        }
        else if (isFullData && isAlbumInTable)
        {
            qDebug() << ""
                        "Album updating...";
            a.id = getAlbumIndex(a);
            qDebug() << "Album id: " << a.id;
            this->updateAlbum(a);
        }else
        {
            qDebug() << "Error: album data is not full. The element with fullname will not be added";
        }
    }
}
