#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "adddialog.h"
#include "editdialog.h"
#include "registerdialog.h"
#include "authdialog.h"
#include "composer.h"
#include <math.h>
#include "storage.h"
#include "xml_storage.h"
#include "sqlite_storage.h"

#include <QListWidgetItem>
#include <QFileDialog>
#include <QMessageBox>
#include <QFileDialog>
#include <QPalette>


Q_DECLARE_METATYPE(Composer*)
Q_DECLARE_METATYPE(Composer)

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{

    ui->setupUi(this);
    ui->composersLabel->setText("<b>Composers:</b>");
    ui->selectedComposerLabel->setText("<b>Selected composer:</b>");


    this->setAllButtonsEnabled(false);

        this->page_size = 3;
        this->setWindowTitle("Composer Database");

    connect(ui->actionCreate_an_account, &QAction::triggered, this, &MainWindow::registrateUserData);
    connect(ui->actionOpen_Storage, &QAction::triggered, this, &MainWindow::openNewStorage);
    connect(ui->actionExit, &QAction::triggered, this, &MainWindow::beforeExit);
    connect(ui->actionComposers_from_XML, &QAction::triggered, this, &MainWindow::importComposersFromXML);
    connect(ui->actionAlbums_from_XML, &QAction::triggered, this, &MainWindow::importAlbumsFromXML);
    connect(ui->actionto_XML, &QAction::triggered, this, &MainWindow::exportComposersToXML);
    connect(ui->actionAlbums_to_XML, &QAction::triggered, this, &MainWindow::exportAlbumsToXML);
    connect(ui->actionLog_in, &QAction::triggered, this, &MainWindow::logIn);
    connect(ui->actionLog_out, &QAction::triggered, this, &MainWindow::logOut);
}

MainWindow::~MainWindow()
{
    if (this->storage != nullptr)
    {
        this->storage->close();
        delete storage;
    }
    delete ui;
}

//extra funcs
int ceil(int number1, int number2)
{
    int c = 0;
    c = number1/number2;
    if ( number1 % number2 > 0)
    {
        c++;
    }
    return c;
}
Composer copy(Composer * composer)
{
    Composer c;

    c.id = composer->id;
    c.fullname = composer->fullname;
//    c.composition = composer->composition;
    c.year = composer->year;
    c.amount = composer->amount;

    return c;

}

//qr code
void getCode_Qr(QString website, const char * slot, QObject * parent)
{
    QString text = QUrl::toPercentEncoding(website);
    QNetworkAccessManager * manager = new QNetworkAccessManager(parent);
    QObject::connect(manager, SIGNAL(finished(QNetworkReply*)), parent, slot);
    QUrl url("http://api.qrserver.com/v1/create-qr-code/?data=" + text + "&size=180x180");
    QNetworkRequest request(url);
    manager->get(request);
}
void MainWindow::on_printQR(QNetworkReply * reply) {
    QVariant statusCode;
    statusCode = reply->attribute(QNetworkRequest::HttpStatusCodeAttribute);
    if (!statusCode.isValid()) {
        QMessageBox box;
        box.critical(0, "HTTP ERROR", "Status code is not valid");
        return;
    }
    if (statusCode.toInt() != 200) {
        QString error = "Error on " + reply->url().toString() + "\n";
        error += reply->attribute(QNetworkRequest::HttpReasonPhraseAttribute).toString();
        QMessageBox box;
        box.critical(0, "HTTP ERROR", error);
        return;
    }
    QByteArray pngData = reply->readAll();
    QPixmap pixmap;
    pixmap.loadFromData(pngData);
    pixmap = pixmap.scaled(ui -> qr_label -> size(), Qt::KeepAspectRatio);

    ui->qr_label->show();
    ui->qr_label->setPixmap(pixmap);
}

//user
User MainWindow::logIn()
{
    checkButtons();
    ui->listWidget->clear();
    User user;
    user.id = -1;
    if (this->storage == nullptr)
    {
        QMessageBox::information(
            this,
            "Log in",
            "Please, open the storage!");
        return user;
    }

    AuthDialog authDialog(&user, this->storage);
    authDialog.setWindowTitle("Log in");

    if(!authDialog.exec())
    {
        qDebug() << "Rejected!";
    }
    else
    {
        if( (this->storage->getUserAuth(user.username, user.password_hash)) != nullopt)
        {
            QMessageBox::information(
                this,
                "Log in",
                "Successfull!");

            user = *this->storage->getUserAuth(user.username, user.password_hash);
            this->user_id = user.id;

            ui->currentPage_spinBox->setEnabled(true);
            ui->addButton->setEnabled(true);
            ui->search_lineEdit->setEnabled(true);
            ui->menuImport->setEnabled(true);
            ui->menuExport->setEnabled(true);
            ui->actionLog_out->setEnabled(true);

            ui->usernameOut_label->setText(QString::fromStdString(user.username));

            setGUI();
            return user;
        }
        else
        {
            QMessageBox::information(
                this,
                "Log in",
                "Incorrect username or password");
        }
    }
    ui->actionLog_in->setEnabled(false);
    return user;
}
void MainWindow::logOut()
{
    this->setAllButtonsEnabled(false);
    ui->listWidget->clear();
    ui->qr_label->clear();
    ui->fullname_labelOut->clear();
    ui->amount_labelOut->clear();
    ui->year_labelOut->clear();

    ui->actionCreate_an_account->setEnabled(true);
    ui->currentPage_spinBox->setValue(0);

    this->pages_total = 0;
    this->user_id = -1;
    ui->actionLog_in->setEnabled(true);
    setGUI();
}
User MainWindow::registrateUserData()
{
    User user;
    user.id = -1;
    if (this->storage == nullptr)
    {
        QMessageBox::information(
            this,
            "Sign in",
            "Please, open the storage!");
        return user;
    }
    qDebug() << "Registration";
    RegisterDialog registerDialog(&user, this->storage);
    registerDialog.setWindowTitle("Sign in");
    int status = registerDialog.exec();
    if (status == 1)
    {
          if(this->storage->registrateUserData(user.username, user.password_hash) != nullopt)
          {
              QMessageBox::information(
                  this,
                  "Sign in",
                  "Successfull!");
              user = *this->storage->getUserAuth(user.username, user.password_hash);
              this->user_id = user.id;

              if (user.id == -1)
              {
                  qDebug() << "Error: user id = -1";
              }
              return user;
          }
           else
          {
              QMessageBox::warning(
                  this,
                  "Sigh in",
                  "Error: Such username has already exists in database");
          }
     }
    else
    {
              qDebug() << "Rejected!";
    }
    return user;
}

//buttons actions
void MainWindow::on_removeButton_clicked()
{
    QList<QListWidgetItem *> items = ui->listWidget->selectedItems();
    if(items.count() == 0)
    {
        qDebug() << "nothing to remove";
    }
    else
    {
        QMessageBox::StandardButton reply;
        reply = QMessageBox::question(
            this,
            "On delete",
            "Are you sure?",
            QMessageBox::Yes|QMessageBox::No);
        if (reply == QMessageBox::Yes) {

            foreach(QListWidgetItem * selectedItem, items)
            {
                int row_index = ui->listWidget->row(selectedItem);
                ui->listWidget->takeItem(row_index);
                QVariant var = selectedItem->data(Qt::UserRole);
                Composer *comp = static_cast<Composer *>(var.data());

                this->storage->removeComposer(comp->id);
                this->storage->removeImage(comp->id);
                delete selectedItem;
            }

        } else {
            qDebug() << "Yes was *not* clicked";
        }
    }
    setAllPages();
}
void MainWindow::on_addButton_clicked()
{
    bool isEntered = true;
    do
    {
        Composer comp;
        AddDialog addDialog(&comp);
        addDialog.setWindowTitle("Add");
        int status = addDialog.exec();

        if (status == 1)
        {
            if(comp.fullname.size() == 0 || comp.year == 0) //comp.composition.size() == 0 ||
            {
                isEntered = false;
                QMessageBox::warning(
                    this,
                    "Add",
                    "Some filds are empty. Please, enter the correct data!");
            }else
            {
                isEntered = true;

                int id = this->storage->insertComposer(comp);
                if(id != -1)
                {
                    comp.id = id;

                    QVariant var;
                    var.setValue(comp);

                    setGUI();
                    if (addDialog.filename.size() != 0)
                    {
                        string fileway = addDialog.filename.toStdString();
                        this->storage->insertImage(comp.id, fileway);
                    }
                }
                else
                {
                    QMessageBox::warning(
                        this,
                        "Add",
                        "Composer with the same name has already existed in the database.");
                }

            }
        }
        else
        {
            isEntered = true;
            qDebug() << "Rejected!";
        }
    } while(!isEntered);

    setAllPages();
    ui->editButton->setEnabled(false);
    ui->removeButton->setEnabled(false);
}
void MainWindow::on_editButton_clicked()
{
    QList<QListWidgetItem *> items = ui->listWidget->selectedItems();

    if(items.count() == 0)
    {
        qDebug() << "Error: Nothing to edit";
        return;
    }
    QListWidgetItem * selectedComposer = items.at(0);
    QVariant var = selectedComposer->data(Qt::UserRole);
    Composer * comp = static_cast<Composer*>(var.data());
    vector <Album> albums;
    albums = this->storage->getAllComposerAlbums(comp->id);
    bool isEntered = true;
    do
    {
        EditDialog editDialog(comp, this->storage);

        editDialog.setWindowTitle("Edit");
        editDialog.presentValuesInEditDialog();
        int status = editDialog.exec();

        if (status == 1)
        {
            if(comp->fullname.size() != 0) // && comp->composition.size() != 0)
            {
                isEntered = true;
                var.setValue(copy(comp));

                selectedComposer->setText(comp->fullname.c_str());
                selectedComposer->setData(Qt::UserRole, var);

                this->storage->updateComposer(copy(comp));
            }
            else
            {
                isEntered = false;
                QMessageBox::warning(
                this,
                "Add",
                "Some filds are empty. Please, enter the correct data!");
            }
        } else
        {
            isEntered = true;
            qDebug() << "Rejected!";
        }
    } while(!isEntered);
}

void MainWindow::on_nextButton_clicked()
{
    ui->listWidget->clear();
    int current_page = ui->currentPage_spinBox->text().toInt();
    current_page += 1;
    ui->currentPage_spinBox->setValue(current_page);

    ui->allPages_label->setText("/" + QString::number(this->pages_total));
    ui->editButton->setEnabled(false);
    ui->removeButton->setEnabled(false);
    setGUI();
}
void MainWindow::on_previousButton_clicked()
{
    ui->listWidget->clear();
    int current_page = ui->currentPage_spinBox->text().toInt();
    current_page -= 1;
    ui->currentPage_spinBox->setValue(current_page);

    ui->editButton->setEnabled(false);
    ui->removeButton->setEnabled(false);
    setGUI();
}


void MainWindow::on_search_lineEdit_textChanged()
{
    setGUI();
}

QString createURLlink(QString & fullname)
{
    QString string = "https://en.wikipedia.org/wiki/";
    fullname = fullname.simplified();
    fullname.replace( " ", "_" );
    qDebug() << fullname;
    string += fullname;
    qDebug() << string;
    return string;
}

void MainWindow::on_listWidget_itemClicked(QListWidgetItem *item)
{
    bool isOnlyOneItemSelected= false;
    bool isSelected = false;
    if (ui->listWidget->selectedItems().size() >= 1)
    {
        isSelected = true;
        if(ui->listWidget->selectedItems().size() == 1)
        {
            isOnlyOneItemSelected = true;
        }
    }
    ui->editButton->setEnabled(isOnlyOneItemSelected);
    ui->removeButton->setEnabled(isSelected);
    if (item != nullptr)
    {
        QVariant var = item->data(Qt::UserRole);
        Composer *comp = static_cast<Composer*>(var.data());
        ui->fullname_labelOut->setText(QString::fromStdString(comp->fullname));
        ui->year_labelOut->setText(QString::number(comp->year));
        ui->amount_labelOut->setText(QString::number(comp->amount));

        QString fullname = QString::fromStdString(comp->fullname);
        QString wikiInform = createURLlink(fullname);

        getCode_Qr(wikiInform, SLOT(on_printQR(QNetworkReply*)), this);

        vector <Album> albums = this->storage->getAllComposerAlbums(comp->id);
        if (albums.size() != 0)
        {
            string al_name = albums.at(0).fullname;
            QString string = QString::fromStdString(al_name);
            QString nums = QString::number(albums.size()-1);
            if(string.size() != 0)
            {
                ui->albumOut_label->setText(string + "(+" + nums + ")");
            }

        }else
        {
            ui->albumOut_label->clear();
        }
        QString filename = this->storage->getComposerImage(comp->id);
        if (setImage(filename) == false)
        {
            ui->image_label->clear();
        }


    }
}

void MainWindow::beforeExit()
{
   QMessageBox::StandardButton reply;
   reply = QMessageBox::question(
       this,
       "On delete",
       tr("Are you sure?"),
       QMessageBox::Yes|QMessageBox::No);
   if (reply == QMessageBox::Yes) {
       qDebug() << "Yes was clicked";
       close();
   } else {
       qDebug() << "Yes was *not* clicked";
       return;
   }
}

// gui updating
void MainWindow::setAllPages()
{
    int items_total = this->storage->getNumberOfUserComposers(user_id);
    int page_size = this->page_size;
    this->pages_total = ceil(items_total, page_size);
    ui->allPages_label->setText("/" + QString::number(this->pages_total));

    if (!ui->search_lineEdit->text().isEmpty())
    {

    }

}
void MainWindow::setAllButtonsEnabled(bool state)
{
    ui->actionLog_in->setEnabled(state);
    ui->actionSign_up->setEnabled(state);
    ui->nextButton->setEnabled(state);
    ui->previousButton->setEnabled(state);
    ui->addButton->setEnabled(state);
    ui->editButton->setEnabled(state);
    ui->removeButton->setEnabled(state);
    ui->currentPage_spinBox->setEnabled(state);
    ui->search_lineEdit->setEnabled(state);
    ui->menuImport->setEnabled(state);
    ui->menuExport->setEnabled(state);
    ui->actionLog_out->setEnabled(state);

    ui->information_label->clear();
    ui->actionCreate_an_account->setEnabled(state);

}
bool MainWindow::setImage(QString &filename)
{
    if (filename != nullptr)
    {
        QImage image;
        bool valid = image.load(filename);
        if(valid)
        {
            image = image.scaledToWidth(ui->image_label->width(), Qt::SmoothTransformation);
            image = image.scaledToHeight(ui->image_label->height(), Qt::SmoothTransformation);
            ui->image_label->setPixmap(QPixmap::fromImage(image));
            return true;
        }else
        {
            ui->image_label->clear();
        }
    }
    return false;
}
void MainWindow::checkButtons()
{

    if (this->storage == nullptr)
    {
        setAllButtonsEnabled(false);
    }

    if (this->storage != nullptr && this->user_id == -1)
    {
        ui->actionLog_in->setEnabled(true);
        ui->actionCreate_an_account->setEnabled(true);
    }

    if (this->storage != nullptr && this->user_id != -1)
    {
        ui->information_label->setText("To get more information:");
        ui->menuImport->setEnabled(true);
        ui->menuExport->setEnabled(true);
        ui->actionLog_in->setEnabled(false);

        int page_number = ui->currentPage_spinBox->text().toInt();

        if(page_number >= this->pages_total)
        {
            ui->nextButton->setEnabled(false);
        }else
        {
            ui->nextButton->setEnabled(true);
        }
        if(page_number == 1)
        {
            ui->previousButton->setEnabled(false);
        }else
        {
            ui->previousButton->setEnabled(true);
        }
    }
}
void MainWindow::setGUI()
{
    ui->listWidget->clear();
    setAllPages();
    checkButtons();
    int page_number = ui->currentPage_spinBox->text().toInt();

    vector <Composer> composers;

    if (ui->search_lineEdit->text().size() == 0)
    {
        composers = this->storage->getSomeUserComposers(this->user_id, this->page_size, page_number);
    }else
    {
        string toSearch = ui->search_lineEdit->text().toStdString().c_str();
        composers = this->storage->getSeachedUserComposers(this->user_id, this->page_size,page_number,toSearch);

        int page_size = this->page_size;
        int pages_total = ceil(composers.size(), page_size);

        ui->allPages_label->setText("/" + QString::number(pages_total));
    }

    foreach(Composer c, composers)
    {
            QVariant var;
            var.setValue(c);

            QListWidgetItem * newComposerListItem = new QListWidgetItem();
            newComposerListItem->setText(c.fullname.c_str());
            newComposerListItem->setData(Qt::UserRole, var);
            ui->listWidget->addItem(newComposerListItem);
    }
}

//storage

void MainWindow::openNewStorage()
{
   QString fileName = QFileDialog::getOpenFileName(
               this,              // parent
               "Open",  // caption
               "../../src/data/sql",                // directory to start with
               "SQL (*.sqlite);;All Files (*)");  // file name filter
   if (fileName != nullptr && !fileName.isEmpty())
   {
       ui->listWidget->clear();

       SqliteStorage * sqlite_storage = new SqliteStorage(fileName.toStdString());
       this->storage = sqlite_storage;
       this->storage->open();
       MainWindow::logIn();
   }
}

// import/export
void MainWindow::importComposersFromXML()
{
    QString fileName = QFileDialog::getOpenFileName(
                this,              // parent
                "Open",  // caption
                "../../src/data/xml",                // directory to start with
                "XML (*.xml);;All Files (*)");  // file name filter
    if (fileName != nullptr && fileName.size() != 0)
    {
        string filename = fileName.toStdString();
        this->storage->importComposersFromXML(filename);
    }
    setGUI();
}
void MainWindow::importAlbumsFromXML()
{
    QString fileName = QFileDialog::getOpenFileName(
                this,              // parent
                "Open",  // caption
                "../../src/data/xml",                // directory to start with
                "XML (*.xml);;All Files (*)");  // file name filter
    if (fileName != nullptr && fileName.size() != 0)
    {
        string filename = fileName.toStdString();
        this->storage->importAlbumsFromXML(filename);
    }
    setGUI();
}

void MainWindow::exportComposersToXML()
{
    QString fileName = QFileDialog::getSaveFileName(
                this,
                tr("Save Composers Database"),
                "../../src/data/xml",
                tr("XML (*.xml);;All Files (*)"));

    if (fileName.isEmpty())
        return;
    else
    {
        string filename = fileName.toStdString();
//        this->storage->exportComposersToXML(filename);
        XmlStorage XmlStorageFile;
        vector <Composer> composers = this->storage->getAllUserComposers(this->user_id);
        XmlStorageFile.writeXmlComposersTextToFile(filename, composers);
    }
}

void MainWindow::exportAlbumsToXML()
{
    QString fileName = QFileDialog::getSaveFileName(
                this,
                tr("Save Composers Database"),
                "../../src/data/xml",
                tr("XML (*.xml);;All Files (*)"));

    if (fileName.isEmpty())
        return;
    else
    {
        string filename = fileName.toStdString();
//        this->storage->exportAlbumsToXML(filename);
        XmlStorage XmlStorageFile;
        vector <Album> albums = this->storage->getAllAlbums();
        XmlStorageFile.writeXmlAlbumsTextToFile(filename, albums);
    }
}
