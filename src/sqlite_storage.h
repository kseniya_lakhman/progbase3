#pragma once
#ifndef SQLITE_STORAGE_H
#define SQLITE_STORAGE_H


#include "storage.h"
#include <QSqlDatabase>

class SqliteStorage: public Storage
{
    const string dir_name_;
    int userId;
    QSqlDatabase db_;

    bool isComposerExist(Composer c);
    bool isAlbumExist(Album a);

    int getComposerIndex(Composer c);
    int getAlbumIndex(Album a);

public:
    SqliteStorage(const string &dir_name);

     bool open();
     bool close();

    // composers
     vector<Composer> getAllUserComposers(int user_id);
     vector<Composer> getAllComposers(void);
     optional<Composer> getComposerById(int composer_id);
     bool updateComposer(const Composer &composer);
     bool removeComposer(int composer_id);
     int insertComposer(const Composer & composer);
     int getNumberOfUserComposers(int user_id);
     int getNumberOfSearchedUserComposers(int user_id, string & search_text);


    // albums
     vector<Album> getAllAlbums(void);
     optional<Album> getAlbumById(int album_id);
     bool updateAlbum(const Album &album);
     bool removeAlbum(int album_id);
     int insertAlbum(const Album & album);

     //users
     optional<User> getUserAuth(string & username, string & password);
     optional<User> registrateUserData(string & username, string & password);

     // links
     vector<Album> getAllComposerAlbums(int composer_id);
     bool insertComposerAlbum(int composer_id, int album_id);
     bool removeComposerAlbum(int composer_id, int album_id);

     //images
     bool insertImage(int composer_id, string & filename);
     bool removeImage(int composer_id);
     QString getComposerImage(int composer_id);
     bool updateComposerImage(int composer_id, string & filename);

     //part of composers
     vector<Composer> getSomeUserComposers(int user_id, int page_size, int page_number);
     vector<Composer> getSeachedUserComposers( int user_id, int page_size, int page_number, string & search_text);

     //import
     void importComposersFromXML(string & filename);
     void importAlbumsFromXML(string & filename);
};

#endif // SQLITE_STORAGE_H
