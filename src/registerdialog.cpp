#include "registerdialog.h"
#include "ui_registerdialog.h"

#include <string>
#include <QDebug>
#include <QCryptographicHash>

RegisterDialog::RegisterDialog(User *user, Storage *storage, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::RegisterDialog)
{

    ui->setupUi(this);
    change = user;
    ui->message_label->setText("<b> Enter your data to sign in: <b/>");
}

RegisterDialog::~RegisterDialog()
{
    delete ui;
}


void RegisterDialog::on_buttonBox_accepted()
{
    change->username = ui->username_lineEdit->text().toStdString();

    string entered_password = ui->password_lineEdit->text().toStdString();
    string enteredAgain_password = ui->passwordAgain_lineEdit->text().toStdString();
    if (entered_password == enteredAgain_password)
    {
        QString hash = QString(QCryptographicHash::hash((entered_password.c_str()),QCryptographicHash::Md5).toHex());
        change->password_hash = hash.toStdString();
    }
    else
    {
        qDebug() << "Error: different passwords";
    }
}

void RegisterDialog::on_password_lineEdit_textChanged(const QString &arg1)
{
    ui->warning_label->clear();
    if (!ui->passwordAgain_lineEdit->text().isEmpty() && !ui->password_lineEdit->text().isEmpty())
    {
        int result = arg1.compare(ui->passwordAgain_lineEdit->text());
        if(result != 0)
        {
            ui->warning_label->setText("Different passwords");

        }else
        {
            ui->warning_label->clear();
        }
    }else
    {
         ui->warning_label->clear();
    }
}
void RegisterDialog::on_passwordAgain_lineEdit_textChanged(const QString &arg1)
{
    ui->warning_label->clear();
    if (!ui->password_lineEdit->text().isEmpty() && !ui->passwordAgain_lineEdit->text().isEmpty() )
    {
        int result = arg1.compare(ui->password_lineEdit->text());
        if(result != 0)
        {
            ui->warning_label->setText("Different passwords");
        }else
        {
            ui->warning_label->clear();
        }
    }else
    {
         ui->warning_label->clear();
    }
}
