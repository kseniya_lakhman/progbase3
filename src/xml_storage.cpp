#include "xml_storage.h"
#include <QString>
#include <QFile>
#include <QTextStream>
#include <QtXml>
#include <QDebug>
#include <string>

QDomDocument readAllFromXmlFile(string const &filename)
{
    QString composers_filename = QString::fromStdString(filename);
    QFile file(composers_filename);
    bool is_composers_opened = file.open(QFile::ReadOnly);

    if (!is_composers_opened)
    {
        cerr << "Can`t open the file to read: " << filename;
        abort();
    }

    //
    QTextStream ts(&file);
    QString text = ts.readAll();

    QDomDocument doc;
    QString errorMessage;
    int errorLine;
    int errorColumn;

    bool is_passed = doc.setContent(text, &errorMessage, &errorLine, &errorColumn);
    if (!is_passed)
    {
        qDebug() << "Error parsing xml: " << errorMessage;
        qDebug() << "in line: " << errorLine;
    }
    file.close();
    return doc;
}
Composer domElementToComposer(QDomElement &element)
{
    Composer c;
    c.id = element.attributeNode("id").value().toInt();
    c.fullname = element.attributeNode("fullname").value().toStdString();
    c.year = element.attributeNode("year").value().toInt();
    c.amount = element.attributeNode("amount").value().toInt();

    return c;
}

Album domElementToAlbum(QDomElement &element)
{
    Album a;
    a.id = element.attributeNode("id").value().toInt();
    a.fullname = element.attributeNode("fullname").value().toStdString();
    a.kind =  element.attributeNode("kind").value().toStdString();
    return a;
}

QDomElement composerToDomElement(QDomDocument &doc, Composer &c)
{

    QDomElement composer_el = doc.createElement("composer");
    composer_el.setAttribute("id", c.id);
    composer_el.setAttribute("fullname", c.fullname.c_str());
    composer_el.setAttribute("year", c.year);
    composer_el.setAttribute("amount", c.amount);

    return composer_el;
}
QDomElement albumToDomElement(QDomDocument &doc, Album &a)
{

    QDomElement album_el = doc.createElement("album");
    album_el.setAttribute("id", a.id);
    album_el.setAttribute("fullname", a.fullname.c_str());
    album_el.setAttribute("kind", a.kind.c_str());
    return album_el;
}

//export
QString createXmlComposersText(vector<Composer> &composers)
{
    QDomDocument composers_doc;
    QDomElement composers_root = composers_doc.createElement("composers");
    for (Composer &composer : composers)
    {
        QDomElement composer_el = composerToDomElement(composers_doc, composer);
        composers_root.appendChild(composer_el);
    }

    composers_doc.appendChild(composers_root);
    QString composers_xml_text = composers_doc.toString(4);
    return composers_xml_text;
}

QString createXmlAlbumsText(vector<Album> &albums)
{
    QDomDocument albums_doc;
    QDomElement albums_root = albums_doc.createElement("albums");
    for (Album &album : albums)
    {
        QDomElement album_el = albumToDomElement(albums_doc, album);
        albums_root.appendChild(album_el);
    }

    albums_doc.appendChild(albums_root);
    QString albums_xml_text = albums_doc.toString(4);
    return albums_xml_text;
}

void XmlStorage::writeXmlComposersTextToFile(string const &filename, vector<Composer> &composers)
{
    QString xml_text = createXmlComposersText(composers);


    QString mainFilename = QString::fromStdString(filename);

    QFile file(mainFilename);
    if (!file.open(QFile::WriteOnly))
    {
        qDebug() << "Can`t open file to write: " << mainFilename;
        abort();
    }

    QTextStream ts(&file);
    ts << xml_text;
    file.close();
}

void XmlStorage::writeXmlAlbumsTextToFile(string const &filename, vector<Album> &albums)
{
    QString xml_text = createXmlAlbumsText(albums);


    QString mainFilename = QString::fromStdString(filename);

    QFile file(mainFilename);
    if (!file.open(QFile::WriteOnly))
    {
        qDebug() << "Can`t open file to write: " << mainFilename;
        abort();
    }

    QTextStream ts(&file);
    ts << xml_text;
    file.close();
}

vector<Composer> XmlStorage::getAllComposersFromXML(string &filename)
{
    vector<Composer> composers;
    QDomDocument composers_doc = readAllFromXmlFile(filename);
    QDomElement composers_root = composers_doc.documentElement();

    for (int i = 0; i < composers_root.childNodes().size(); i++)
    {
        QDomNode composers_node = composers_root.childNodes().at(i);
        if(composers_node.isElement())
        {
            QDomElement composers_element = composers_node.toElement();
            Composer composer = domElementToComposer(composers_element);
            composers.push_back(composer);
        }
    }

    return composers;
}
vector<Album> XmlStorage::getAllAlbumsFromXML(string &filename)
{
    vector<Album> albums;
    QDomDocument albums_doc = readAllFromXmlFile(filename);
    QDomElement albums_root = albums_doc.documentElement();

    for (int i = 0; i < albums_root.childNodes().size(); i++)
    {
        QDomNode albums_node = albums_root.childNodes().at(i);
        if(albums_node.isElement())
        {
            QDomElement albums_element = albums_node.toElement();
            Album album = domElementToAlbum(albums_element);
            albums.push_back(album);
        }
    }

    return albums;
}
