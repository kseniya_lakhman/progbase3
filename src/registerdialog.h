#ifndef REGISTERDIALOG_H
#define REGISTERDIALOG_H

#include <QDialog>
#include "storage.h"

namespace Ui {
class RegisterDialog;
}

class RegisterDialog : public QDialog
{
    Q_OBJECT

public:
    explicit RegisterDialog(User *user,  Storage *storage, QWidget *parent = 0);
    ~RegisterDialog();

private slots:
    void on_password_lineEdit_textChanged(const QString &arg1);

    void on_buttonBox_accepted();

    void on_passwordAgain_lineEdit_textChanged(const QString &arg1);

private:
    Ui::RegisterDialog *ui;
    User * change;
};

#endif // REGISTERDIALOG_H
