#pragma once

#include <vector>
#include <string>
#include <QtXml>
#include "storage.h"

using std::string;
using std::vector;

class XmlStorage
{
  const string dir_name_;

  vector<Composer> composers_;
  vector<Album> albums_;
public:
  vector<Composer> getAllComposersFromXML(string &filename);
  vector<Album> getAllAlbumsFromXML(string &filename);

  void writeXmlComposersTextToFile(string const &filename, vector<Composer> &composers);
  void writeXmlAlbumsTextToFile(string const &filename, vector<Album> &albums);
};
