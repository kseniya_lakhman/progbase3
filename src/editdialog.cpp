#include "editdialog.h"
#include "ui_editdialog.h"
#include <QWidget>
#include <QDebug>
#include <QFileDialog>

Q_DECLARE_METATYPE(Composer*)
Q_DECLARE_METATYPE(Album)

EditDialog::EditDialog(Composer *composer, Storage *storage, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::EditDialog)
{
    ui->setupUi(this);
    change = composer;
    ui->newComposer_label->setText("<b> Edit composer data: <b/>");
    stor = storage;
    setWidgetComposerAlbumsList(stor->getAllComposerAlbums(composer->id));
    setWidgetAllAlbumsList();
}

EditDialog::~EditDialog()
{
    delete ui;
}

void EditDialog::setWidgetAllAlbumsList()
{
    vector <Album> composer_albums = this->stor->getAllComposerAlbums(change->id);
    vector <Album> all_albums = this->stor->getAllAlbums();

    foreach(Album al, all_albums)
    {
        bool isCompNotAdded= false;

        foreach(Album album, composer_albums)
        {
            if(album.id == al.id)
            {
                isCompNotAdded = true;
            }
        }

        if(!isCompNotAdded)
        {
            QVariant var;
            var.setValue(al);

            QListWidgetItem * newAlbumListItem = new QListWidgetItem();
            newAlbumListItem->setText(al.fullname.c_str());
            newAlbumListItem->setData(Qt::UserRole, var);
            ui->allALbums_listWidget->addItem(newAlbumListItem);
        }

    }
}

void EditDialog::setWidgetComposerAlbumsList(vector <Album> albums)
{
    foreach(Album al, albums)
    {
        QVariant var;
        var.setValue(al);

        QListWidgetItem * newAlbumListItem = new QListWidgetItem();
        newAlbumListItem->setText(al.fullname.c_str());
        newAlbumListItem->setData(Qt::UserRole, var);
        ui->compAlbums_listWidget->addItem(newAlbumListItem);
    }
}

void EditDialog::on_buttonBox_accepted()
{
    change->fullname = ui->fullname_lineEdit->text().toStdString();
//    change->composition = ui->composition_LineEdit->text().toStdString();
    change->year = ui->year_spinBox->text().toInt();
    change->amount = ui->amount_spinBox->text().toInt();
}

void EditDialog::presentValuesInEditDialog()
{
    ui->fullname_lineEdit->setText(QString::fromStdString(change->fullname));
//    ui->composition_LineEdit->setText(QString::fromStdString(change->composition));
    ui->year_spinBox->setValue(change->year);
    ui->amount_spinBox->setValue(change->amount);

    qDebug() << change->id << "--- here";
    QString filename = this->stor->getComposerImage(change->id);
    qDebug() << filename.toStdString().c_str()<< "--- filename";
    setImage(filename);
    qDebug() << change->id << "--- change id";
}

void EditDialog::on_add_pushButton_clicked()
{
    QList<QListWidgetItem *> item = ui->allALbums_listWidget->selectedItems();
    if(item.count() == 1)
    {
        QListWidgetItem * selectedAlbum = item.at(0);
        QVariant var = selectedAlbum->data(Qt::UserRole);
        Album * album= static_cast<Album*>(var.data());

        this->stor->insertComposerAlbum(change->id, album->id);

        QListWidgetItem * newComposerListItem = new QListWidgetItem();
        newComposerListItem->setText(album->fullname.c_str());
        newComposerListItem->setData(Qt::UserRole, var);
        ui->compAlbums_listWidget->addItem(newComposerListItem);

        int row_index = ui->allALbums_listWidget->row(selectedAlbum);
        ui->allALbums_listWidget->takeItem(row_index);

        delete  selectedAlbum;
    }
}


void EditDialog::on_delete_pushButton_clicked()
{
    QList<QListWidgetItem *> item = ui->compAlbums_listWidget->selectedItems();
    if(item.count() == 1)
    {
        QListWidgetItem * selectedAlbum = item.at(0);
        QVariant var = selectedAlbum->data(Qt::UserRole);
        Album * album= static_cast<Album*>(var.data());

        QListWidgetItem * newComposerListItem = new QListWidgetItem();
        newComposerListItem->setText(album->fullname.c_str());
        newComposerListItem->setData(Qt::UserRole, var);
        ui->allALbums_listWidget->addItem(newComposerListItem);

        int row_index = ui->compAlbums_listWidget->row(selectedAlbum);
        ui->compAlbums_listWidget->takeItem(row_index);

        this->stor->removeComposerAlbum(change->id, album->id);
        delete  selectedAlbum;
    }
}



void EditDialog::on_allALbums_listWidget_itemClicked(QListWidgetItem *item)
{
    qDebug() << "item clicked on_allALbums! ";
}


void EditDialog::on_compAlbums_listWidget_itemClicked(QListWidgetItem *item)
{
       qDebug() << "item clicked on_compAlbums! ";
}

bool EditDialog::setImage(QString &filename)
{
    qDebug() << "setImage ";
    if (filename != nullptr && filename.size() != 0)
    {
        qDebug() << "setImage (1)";
        QImage image;
        bool valid = image.load(filename);
        if(valid)
        {
            image = image.scaledToWidth(ui->image_label->width(), Qt::SmoothTransformation);
            image = image.scaledToHeight(ui->image_label->height(), Qt::SmoothTransformation);
            ui->image_label->setPixmap(QPixmap::fromImage(image));
            return true;
        }else
        {
            qDebug() << "Error";
        }
    }
    return false;
}

void EditDialog::on_chooseImage_pushButton_clicked()
{
        QString newfileName = QFileDialog::getOpenFileName(
                    this,              // parent
                    "Choose",  // caption
                    "../../src/data/images",                // directory to start with
                    "Images (*.png *.jpg *.jpeg *.gif);;All Files (*)");  // file name filter
        qDebug() << newfileName;
        setImage(newfileName);

        string newFileWay = newfileName.toStdString();
        QString oldFilename = this->stor->getComposerImage(change->id);

        if(oldFilename.size() == 0 && newfileName.size() != 0)
        {
            this->stor->insertImage(change->id, newFileWay);
        }else if (newfileName.size() != 0)
        {
            this->stor->updateComposerImage(change->id, newFileWay);
        }
}
