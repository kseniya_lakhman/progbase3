#-------------------------------------------------
#
# Project created by QtCreator 2019-06-02T12:40:05
#
#-------------------------------------------------

QT       += core gui
QT       += xml
QT       += sql
QT       += network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = src
TEMPLATE = app

DEPENDPATH += . ../storage
INCLUDEPATH += ../storage
# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        main.cpp \
        mainwindow.cpp \
    editdialog.cpp \
    sqlite_storage.cpp \
    authdialog.cpp \
    xml_storage.cpp \
    adddialog.cpp \
    registerdialog.cpp


HEADERS += \
        mainwindow.h \
    editdialog.h \
    sqlite_storage.h \
    authdialog.h \
    xml_storage.h \
    adddialog.h \
    user.h \
    storage.h \
    album.h \
    composer.h \
    optional.h \
    registerdialog.h


FORMS += \
        mainwindow.ui \
    authdialog.ui \
    editdialog.ui \
    adddialog.ui \
    registerdialog.ui
