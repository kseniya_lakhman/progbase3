#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QListWidgetItem>
#include <QDebug>
#include <QTcpSocket>
#include <QNetworkReply>

#include <storage.h>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_removeButton_clicked();
    void on_addButton_clicked();
    void on_editButton_clicked();

    void on_listWidget_itemClicked(QListWidgetItem *item);

    User logIn();
    void logOut();
    void openNewStorage();
    User registrateUserData();
    void beforeExit();
    bool setImage(QString &filename);

    void on_nextButton_clicked();

    void on_previousButton_clicked();

    void on_search_lineEdit_textChanged();

    void importComposersFromXML();
    void importAlbumsFromXML();
    void exportComposersToXML();
    void exportAlbumsToXML();

    void on_printQR(QNetworkReply *reply);

private:
    Ui::MainWindow *ui;
    Storage * storage = nullptr;
    int user_id = -1;
    int page_size;
    int pages_total;

    void setGUI();
    void setAllPages();
    void checkButtons();
    void setAllButtonsEnabled(bool state);
};

#endif // MAINWINDOW_H
