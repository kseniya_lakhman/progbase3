
#ifndef EDITDIALOG_H
#define EDITDIALOG_H

#include <QDialog>
#include <QListWidgetItem>

#include "storage.h"

namespace Ui {
class EditDialog;
}

class EditDialog : public QDialog
{
    Q_OBJECT

public:
    void presentValuesInEditDialog();
    void setWidgetComposerAlbumsList(vector <Album> albums);
    void setWidgetAllAlbumsList();
    explicit EditDialog(Composer *composer, Storage *storage, QWidget *parent = 0);
    QString filename;
    ~EditDialog();

private slots:
    void on_buttonBox_accepted();

    void on_add_pushButton_clicked();

    void on_allALbums_listWidget_itemClicked(QListWidgetItem *item);

    void on_delete_pushButton_clicked();

    void on_compAlbums_listWidget_itemClicked(QListWidgetItem *item);

    void on_chooseImage_pushButton_clicked();

    bool setImage(QString &filename);

private:
    Ui::EditDialog *ui;
    Storage *stor;
    Composer * change;
};

#endif // EDITDIALOG_H
