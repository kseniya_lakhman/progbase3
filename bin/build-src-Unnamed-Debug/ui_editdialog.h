/********************************************************************************
** Form generated from reading UI file 'editdialog.ui'
**
** Created by: Qt User Interface Compiler version 5.9.5
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_EDITDIALOG_H
#define UI_EDITDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_EditDialog
{
public:
    QDialogButtonBox *buttonBox;
    QLabel *compAlbums_label;
    QWidget *horizontalLayoutWidget;
    QHBoxLayout *horizontalLayout;
    QPushButton *add_pushButton;
    QPushButton *delete_pushButton;
    QWidget *horizontalLayoutWidget_2;
    QHBoxLayout *horizontalLayout_2;
    QListWidget *compAlbums_listWidget;
    QListWidget *allALbums_listWidget;
    QLabel *allAlbums_label;
    QLabel *newComposer_label;
    QPushButton *chooseImage_pushButton;
    QWidget *formLayoutWidget;
    QFormLayout *formLayout;
    QLabel *fullname_Label;
    QLineEdit *fullname_lineEdit;
    QLabel *year_Label;
    QSpinBox *year_spinBox;
    QLabel *amount_Label;
    QSpinBox *amount_spinBox;
    QLabel *image_label;

    void setupUi(QDialog *EditDialog)
    {
        if (EditDialog->objectName().isEmpty())
            EditDialog->setObjectName(QStringLiteral("EditDialog"));
        EditDialog->resize(559, 577);
        EditDialog->setStyleSheet(QStringLiteral("font: 11pt \"URW Bookman L\";"));
        buttonBox = new QDialogButtonBox(EditDialog);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setGeometry(QRect(-10, 540, 561, 31));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);
        compAlbums_label = new QLabel(EditDialog);
        compAlbums_label->setObjectName(QStringLiteral("compAlbums_label"));
        compAlbums_label->setGeometry(QRect(10, 271, 261, 21));
        horizontalLayoutWidget = new QWidget(EditDialog);
        horizontalLayoutWidget->setObjectName(QStringLiteral("horizontalLayoutWidget"));
        horizontalLayoutWidget->setGeometry(QRect(10, 480, 541, 41));
        horizontalLayout = new QHBoxLayout(horizontalLayoutWidget);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        add_pushButton = new QPushButton(horizontalLayoutWidget);
        add_pushButton->setObjectName(QStringLiteral("add_pushButton"));

        horizontalLayout->addWidget(add_pushButton);

        delete_pushButton = new QPushButton(horizontalLayoutWidget);
        delete_pushButton->setObjectName(QStringLiteral("delete_pushButton"));

        horizontalLayout->addWidget(delete_pushButton);

        horizontalLayoutWidget_2 = new QWidget(EditDialog);
        horizontalLayoutWidget_2->setObjectName(QStringLiteral("horizontalLayoutWidget_2"));
        horizontalLayoutWidget_2->setGeometry(QRect(10, 301, 541, 171));
        horizontalLayout_2 = new QHBoxLayout(horizontalLayoutWidget_2);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        horizontalLayout_2->setContentsMargins(0, 0, 0, 0);
        compAlbums_listWidget = new QListWidget(horizontalLayoutWidget_2);
        compAlbums_listWidget->setObjectName(QStringLiteral("compAlbums_listWidget"));

        horizontalLayout_2->addWidget(compAlbums_listWidget);

        allALbums_listWidget = new QListWidget(horizontalLayoutWidget_2);
        allALbums_listWidget->setObjectName(QStringLiteral("allALbums_listWidget"));

        horizontalLayout_2->addWidget(allALbums_listWidget);

        allAlbums_label = new QLabel(EditDialog);
        allAlbums_label->setObjectName(QStringLiteral("allAlbums_label"));
        allAlbums_label->setGeometry(QRect(290, 270, 161, 21));
        newComposer_label = new QLabel(EditDialog);
        newComposer_label->setObjectName(QStringLiteral("newComposer_label"));
        newComposer_label->setGeometry(QRect(10, 20, 361, 22));
        chooseImage_pushButton = new QPushButton(EditDialog);
        chooseImage_pushButton->setObjectName(QStringLiteral("chooseImage_pushButton"));
        chooseImage_pushButton->setGeometry(QRect(10, 240, 171, 21));
        formLayoutWidget = new QWidget(EditDialog);
        formLayoutWidget->setObjectName(QStringLiteral("formLayoutWidget"));
        formLayoutWidget->setGeometry(QRect(210, 50, 341, 121));
        formLayout = new QFormLayout(formLayoutWidget);
        formLayout->setObjectName(QStringLiteral("formLayout"));
        formLayout->setContentsMargins(0, 0, 0, 0);
        fullname_Label = new QLabel(formLayoutWidget);
        fullname_Label->setObjectName(QStringLiteral("fullname_Label"));

        formLayout->setWidget(0, QFormLayout::LabelRole, fullname_Label);

        fullname_lineEdit = new QLineEdit(formLayoutWidget);
        fullname_lineEdit->setObjectName(QStringLiteral("fullname_lineEdit"));

        formLayout->setWidget(0, QFormLayout::FieldRole, fullname_lineEdit);

        year_Label = new QLabel(formLayoutWidget);
        year_Label->setObjectName(QStringLiteral("year_Label"));

        formLayout->setWidget(1, QFormLayout::LabelRole, year_Label);

        year_spinBox = new QSpinBox(formLayoutWidget);
        year_spinBox->setObjectName(QStringLiteral("year_spinBox"));
        year_spinBox->setMinimum(1500);
        year_spinBox->setMaximum(2020);
        year_spinBox->setValue(1900);

        formLayout->setWidget(1, QFormLayout::FieldRole, year_spinBox);

        amount_Label = new QLabel(formLayoutWidget);
        amount_Label->setObjectName(QStringLiteral("amount_Label"));

        formLayout->setWidget(2, QFormLayout::LabelRole, amount_Label);

        amount_spinBox = new QSpinBox(formLayoutWidget);
        amount_spinBox->setObjectName(QStringLiteral("amount_spinBox"));
        amount_spinBox->setMinimum(1);
        amount_spinBox->setMaximum(1000);

        formLayout->setWidget(2, QFormLayout::FieldRole, amount_spinBox);

        image_label = new QLabel(EditDialog);
        image_label->setObjectName(QStringLiteral("image_label"));
        image_label->setGeometry(QRect(10, 50, 191, 181));

        retranslateUi(EditDialog);
        QObject::connect(buttonBox, SIGNAL(accepted()), EditDialog, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), EditDialog, SLOT(reject()));

        QMetaObject::connectSlotsByName(EditDialog);
    } // setupUi

    void retranslateUi(QDialog *EditDialog)
    {
        EditDialog->setWindowTitle(QApplication::translate("EditDialog", "Dialog", Q_NULLPTR));
        compAlbums_label->setText(QApplication::translate("EditDialog", "Composer`s albums:", Q_NULLPTR));
        add_pushButton->setText(QApplication::translate("EditDialog", "Add", Q_NULLPTR));
        delete_pushButton->setText(QApplication::translate("EditDialog", "Delete", Q_NULLPTR));
        allAlbums_label->setText(QApplication::translate("EditDialog", "All albums:", Q_NULLPTR));
        newComposer_label->setText(QApplication::translate("EditDialog", "Edit composer data::", Q_NULLPTR));
        chooseImage_pushButton->setText(QApplication::translate("EditDialog", "Choose the photo", Q_NULLPTR));
        fullname_Label->setText(QApplication::translate("EditDialog", "Fullname", Q_NULLPTR));
        year_Label->setText(QApplication::translate("EditDialog", "Year", Q_NULLPTR));
        amount_Label->setText(QApplication::translate("EditDialog", "Amount", Q_NULLPTR));
        image_label->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class EditDialog: public Ui_EditDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_EDITDIALOG_H
