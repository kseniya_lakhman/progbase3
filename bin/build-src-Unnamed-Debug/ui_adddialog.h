/********************************************************************************
** Form generated from reading UI file 'adddialog.ui'
**
** Created by: Qt User Interface Compiler version 5.9.5
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ADDDIALOG_H
#define UI_ADDDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_AddDialog
{
public:
    QDialogButtonBox *buttonBox;
    QWidget *formLayoutWidget;
    QFormLayout *formLayout;
    QLabel *fullname_Label;
    QLineEdit *fullname_lineEdit;
    QLabel *year_Label;
    QSpinBox *year_spinBox;
    QLabel *amount_Label;
    QSpinBox *amount_spinBox;
    QLabel *newComposer_label;
    QWidget *verticalLayoutWidget;
    QVBoxLayout *verticalLayout;
    QLabel *image_label;
    QPushButton *chooseImage_Button;

    void setupUi(QDialog *AddDialog)
    {
        if (AddDialog->objectName().isEmpty())
            AddDialog->setObjectName(QStringLiteral("AddDialog"));
        AddDialog->resize(626, 335);
        AddDialog->setStyleSheet(QStringLiteral("font: 11pt \"URW Bookman L\";"));
        buttonBox = new QDialogButtonBox(AddDialog);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setGeometry(QRect(320, 290, 291, 32));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);
        formLayoutWidget = new QWidget(AddDialog);
        formLayoutWidget->setObjectName(QStringLiteral("formLayoutWidget"));
        formLayoutWidget->setGeometry(QRect(260, 70, 351, 111));
        formLayout = new QFormLayout(formLayoutWidget);
        formLayout->setObjectName(QStringLiteral("formLayout"));
        formLayout->setContentsMargins(0, 0, 0, 0);
        fullname_Label = new QLabel(formLayoutWidget);
        fullname_Label->setObjectName(QStringLiteral("fullname_Label"));

        formLayout->setWidget(0, QFormLayout::LabelRole, fullname_Label);

        fullname_lineEdit = new QLineEdit(formLayoutWidget);
        fullname_lineEdit->setObjectName(QStringLiteral("fullname_lineEdit"));

        formLayout->setWidget(0, QFormLayout::FieldRole, fullname_lineEdit);

        year_Label = new QLabel(formLayoutWidget);
        year_Label->setObjectName(QStringLiteral("year_Label"));

        formLayout->setWidget(1, QFormLayout::LabelRole, year_Label);

        year_spinBox = new QSpinBox(formLayoutWidget);
        year_spinBox->setObjectName(QStringLiteral("year_spinBox"));
        year_spinBox->setMinimum(1500);
        year_spinBox->setMaximum(2020);
        year_spinBox->setValue(1900);

        formLayout->setWidget(1, QFormLayout::FieldRole, year_spinBox);

        amount_Label = new QLabel(formLayoutWidget);
        amount_Label->setObjectName(QStringLiteral("amount_Label"));

        formLayout->setWidget(2, QFormLayout::LabelRole, amount_Label);

        amount_spinBox = new QSpinBox(formLayoutWidget);
        amount_spinBox->setObjectName(QStringLiteral("amount_spinBox"));
        amount_spinBox->setMinimum(1);
        amount_spinBox->setMaximum(1000);
        amount_spinBox->setValue(50);

        formLayout->setWidget(2, QFormLayout::FieldRole, amount_spinBox);

        newComposer_label = new QLabel(AddDialog);
        newComposer_label->setObjectName(QStringLiteral("newComposer_label"));
        newComposer_label->setGeometry(QRect(10, 30, 331, 21));
        verticalLayoutWidget = new QWidget(AddDialog);
        verticalLayoutWidget->setObjectName(QStringLiteral("verticalLayoutWidget"));
        verticalLayoutWidget->setGeometry(QRect(10, 70, 231, 201));
        verticalLayout = new QVBoxLayout(verticalLayoutWidget);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        image_label = new QLabel(verticalLayoutWidget);
        image_label->setObjectName(QStringLiteral("image_label"));

        verticalLayout->addWidget(image_label);

        chooseImage_Button = new QPushButton(verticalLayoutWidget);
        chooseImage_Button->setObjectName(QStringLiteral("chooseImage_Button"));

        verticalLayout->addWidget(chooseImage_Button);


        retranslateUi(AddDialog);
        QObject::connect(buttonBox, SIGNAL(accepted()), AddDialog, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), AddDialog, SLOT(reject()));

        QMetaObject::connectSlotsByName(AddDialog);
    } // setupUi

    void retranslateUi(QDialog *AddDialog)
    {
        AddDialog->setWindowTitle(QApplication::translate("AddDialog", "AddDialog", Q_NULLPTR));
        fullname_Label->setText(QApplication::translate("AddDialog", "Fullname", Q_NULLPTR));
        year_Label->setText(QApplication::translate("AddDialog", "Year", Q_NULLPTR));
        amount_Label->setText(QApplication::translate("AddDialog", "Amount", Q_NULLPTR));
        newComposer_label->setText(QApplication::translate("AddDialog", "Add new composer:", Q_NULLPTR));
        image_label->setText(QString());
        chooseImage_Button->setText(QApplication::translate("AddDialog", "Choose the photo", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class AddDialog: public Ui_AddDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ADDDIALOG_H
