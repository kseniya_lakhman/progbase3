/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.9.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../src/mainwindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.9.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_MainWindow_t {
    QByteArrayData data[27];
    char stringdata0[410];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MainWindow_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MainWindow_t qt_meta_stringdata_MainWindow = {
    {
QT_MOC_LITERAL(0, 0, 10), // "MainWindow"
QT_MOC_LITERAL(1, 11, 23), // "on_removeButton_clicked"
QT_MOC_LITERAL(2, 35, 0), // ""
QT_MOC_LITERAL(3, 36, 20), // "on_addButton_clicked"
QT_MOC_LITERAL(4, 57, 21), // "on_editButton_clicked"
QT_MOC_LITERAL(5, 79, 25), // "on_listWidget_itemClicked"
QT_MOC_LITERAL(6, 105, 16), // "QListWidgetItem*"
QT_MOC_LITERAL(7, 122, 4), // "item"
QT_MOC_LITERAL(8, 127, 5), // "logIn"
QT_MOC_LITERAL(9, 133, 4), // "User"
QT_MOC_LITERAL(10, 138, 6), // "logOut"
QT_MOC_LITERAL(11, 145, 14), // "openNewStorage"
QT_MOC_LITERAL(12, 160, 18), // "registrateUserData"
QT_MOC_LITERAL(13, 179, 10), // "beforeExit"
QT_MOC_LITERAL(14, 190, 8), // "setImage"
QT_MOC_LITERAL(15, 199, 8), // "QString&"
QT_MOC_LITERAL(16, 208, 8), // "filename"
QT_MOC_LITERAL(17, 217, 21), // "on_nextButton_clicked"
QT_MOC_LITERAL(18, 239, 25), // "on_previousButton_clicked"
QT_MOC_LITERAL(19, 265, 30), // "on_search_lineEdit_textChanged"
QT_MOC_LITERAL(20, 296, 22), // "importComposersFromXML"
QT_MOC_LITERAL(21, 319, 19), // "importAlbumsFromXML"
QT_MOC_LITERAL(22, 339, 20), // "exportComposersToXML"
QT_MOC_LITERAL(23, 360, 17), // "exportAlbumsToXML"
QT_MOC_LITERAL(24, 378, 10), // "on_printQR"
QT_MOC_LITERAL(25, 389, 14), // "QNetworkReply*"
QT_MOC_LITERAL(26, 404, 5) // "reply"

    },
    "MainWindow\0on_removeButton_clicked\0\0"
    "on_addButton_clicked\0on_editButton_clicked\0"
    "on_listWidget_itemClicked\0QListWidgetItem*\0"
    "item\0logIn\0User\0logOut\0openNewStorage\0"
    "registrateUserData\0beforeExit\0setImage\0"
    "QString&\0filename\0on_nextButton_clicked\0"
    "on_previousButton_clicked\0"
    "on_search_lineEdit_textChanged\0"
    "importComposersFromXML\0importAlbumsFromXML\0"
    "exportComposersToXML\0exportAlbumsToXML\0"
    "on_printQR\0QNetworkReply*\0reply"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MainWindow[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      18,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,  104,    2, 0x08 /* Private */,
       3,    0,  105,    2, 0x08 /* Private */,
       4,    0,  106,    2, 0x08 /* Private */,
       5,    1,  107,    2, 0x08 /* Private */,
       8,    0,  110,    2, 0x08 /* Private */,
      10,    0,  111,    2, 0x08 /* Private */,
      11,    0,  112,    2, 0x08 /* Private */,
      12,    0,  113,    2, 0x08 /* Private */,
      13,    0,  114,    2, 0x08 /* Private */,
      14,    1,  115,    2, 0x08 /* Private */,
      17,    0,  118,    2, 0x08 /* Private */,
      18,    0,  119,    2, 0x08 /* Private */,
      19,    0,  120,    2, 0x08 /* Private */,
      20,    0,  121,    2, 0x08 /* Private */,
      21,    0,  122,    2, 0x08 /* Private */,
      22,    0,  123,    2, 0x08 /* Private */,
      23,    0,  124,    2, 0x08 /* Private */,
      24,    1,  125,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 6,    7,
    0x80000000 | 9,
    QMetaType::Void,
    QMetaType::Void,
    0x80000000 | 9,
    QMetaType::Void,
    QMetaType::Bool, 0x80000000 | 15,   16,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 25,   26,

       0        // eod
};

void MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        MainWindow *_t = static_cast<MainWindow *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->on_removeButton_clicked(); break;
        case 1: _t->on_addButton_clicked(); break;
        case 2: _t->on_editButton_clicked(); break;
        case 3: _t->on_listWidget_itemClicked((*reinterpret_cast< QListWidgetItem*(*)>(_a[1]))); break;
        case 4: { User _r = _t->logIn();
            if (_a[0]) *reinterpret_cast< User*>(_a[0]) = std::move(_r); }  break;
        case 5: _t->logOut(); break;
        case 6: _t->openNewStorage(); break;
        case 7: { User _r = _t->registrateUserData();
            if (_a[0]) *reinterpret_cast< User*>(_a[0]) = std::move(_r); }  break;
        case 8: _t->beforeExit(); break;
        case 9: { bool _r = _t->setImage((*reinterpret_cast< QString(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 10: _t->on_nextButton_clicked(); break;
        case 11: _t->on_previousButton_clicked(); break;
        case 12: _t->on_search_lineEdit_textChanged(); break;
        case 13: _t->importComposersFromXML(); break;
        case 14: _t->importAlbumsFromXML(); break;
        case 15: _t->exportComposersToXML(); break;
        case 16: _t->exportAlbumsToXML(); break;
        case 17: _t->on_printQR((*reinterpret_cast< QNetworkReply*(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 17:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QNetworkReply* >(); break;
            }
            break;
        }
    }
}

const QMetaObject MainWindow::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_MainWindow.data,
      qt_meta_data_MainWindow,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow.stringdata0))
        return static_cast<void*>(this);
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 18)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 18;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 18)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 18;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
