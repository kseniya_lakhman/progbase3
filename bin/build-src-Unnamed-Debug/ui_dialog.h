/********************************************************************************
** Form generated from reading UI file 'dialog.ui'
**
** Created by: Qt User Interface Compiler version 5.9.5
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DIALOG_H
#define UI_DIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Dialog
{
public:
    QDialogButtonBox *buttonBox;
    QWidget *formLayoutWidget;
    QFormLayout *formLayout;
    QLabel *fullname_Label;
    QLineEdit *fullname_lineEdit;
    QLabel *year_Label;
    QSpinBox *year_spinBox;
    QLabel *composition_Label;
    QLineEdit *composition_LineEdit;
    QLabel *amount_Label;
    QSpinBox *amount_spinBox;
    QLabel *newComposer_label;

    void setupUi(QDialog *Dialog)
    {
        if (Dialog->objectName().isEmpty())
            Dialog->setObjectName(QStringLiteral("Dialog"));
        Dialog->resize(457, 304);
        Dialog->setStyleSheet(QStringLiteral("font: 11pt \"URW Bookman L\";"));
        buttonBox = new QDialogButtonBox(Dialog);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setGeometry(QRect(140, 260, 291, 32));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);
        formLayoutWidget = new QWidget(Dialog);
        formLayoutWidget->setObjectName(QStringLiteral("formLayoutWidget"));
        formLayoutWidget->setGeometry(QRect(20, 40, 411, 201));
        formLayout = new QFormLayout(formLayoutWidget);
        formLayout->setObjectName(QStringLiteral("formLayout"));
        formLayout->setContentsMargins(0, 0, 0, 0);
        fullname_Label = new QLabel(formLayoutWidget);
        fullname_Label->setObjectName(QStringLiteral("fullname_Label"));

        formLayout->setWidget(0, QFormLayout::LabelRole, fullname_Label);

        fullname_lineEdit = new QLineEdit(formLayoutWidget);
        fullname_lineEdit->setObjectName(QStringLiteral("fullname_lineEdit"));

        formLayout->setWidget(0, QFormLayout::FieldRole, fullname_lineEdit);

        year_Label = new QLabel(formLayoutWidget);
        year_Label->setObjectName(QStringLiteral("year_Label"));

        formLayout->setWidget(1, QFormLayout::LabelRole, year_Label);

        year_spinBox = new QSpinBox(formLayoutWidget);
        year_spinBox->setObjectName(QStringLiteral("year_spinBox"));
        year_spinBox->setMinimum(1500);
        year_spinBox->setMaximum(2020);
        year_spinBox->setValue(1900);

        formLayout->setWidget(1, QFormLayout::FieldRole, year_spinBox);

        composition_Label = new QLabel(formLayoutWidget);
        composition_Label->setObjectName(QStringLiteral("composition_Label"));

        formLayout->setWidget(2, QFormLayout::LabelRole, composition_Label);

        composition_LineEdit = new QLineEdit(formLayoutWidget);
        composition_LineEdit->setObjectName(QStringLiteral("composition_LineEdit"));

        formLayout->setWidget(2, QFormLayout::FieldRole, composition_LineEdit);

        amount_Label = new QLabel(formLayoutWidget);
        amount_Label->setObjectName(QStringLiteral("amount_Label"));

        formLayout->setWidget(3, QFormLayout::LabelRole, amount_Label);

        amount_spinBox = new QSpinBox(formLayoutWidget);
        amount_spinBox->setObjectName(QStringLiteral("amount_spinBox"));
        amount_spinBox->setMinimum(1);
        amount_spinBox->setMaximum(1000);
        amount_spinBox->setValue(50);

        formLayout->setWidget(3, QFormLayout::FieldRole, amount_spinBox);

        newComposer_label = new QLabel(Dialog);
        newComposer_label->setObjectName(QStringLiteral("newComposer_label"));
        newComposer_label->setGeometry(QRect(20, 10, 331, 21));

        retranslateUi(Dialog);
        QObject::connect(buttonBox, SIGNAL(accepted()), Dialog, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), Dialog, SLOT(reject()));

        QMetaObject::connectSlotsByName(Dialog);
    } // setupUi

    void retranslateUi(QDialog *Dialog)
    {
        Dialog->setWindowTitle(QApplication::translate("Dialog", "Dialog", Q_NULLPTR));
        fullname_Label->setText(QApplication::translate("Dialog", "Fullname", Q_NULLPTR));
        year_Label->setText(QApplication::translate("Dialog", "Year", Q_NULLPTR));
        composition_Label->setText(QApplication::translate("Dialog", "Composition", Q_NULLPTR));
        composition_LineEdit->setPlaceholderText(QString());
        amount_Label->setText(QApplication::translate("Dialog", "Amount", Q_NULLPTR));
        newComposer_label->setText(QApplication::translate("Dialog", "Add new composer:", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class AddDialog: public Ui_Dialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DIALOG_H
