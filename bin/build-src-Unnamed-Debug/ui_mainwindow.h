/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.9.5
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionNew_storage;
    QAction *actionOpen_Storage;
    QAction *actionExit;
    QAction *actionSign_up;
    QAction *actionCreate_an_account;
    QAction *actionLog_in;
    QAction *actionLog_out;
    QAction *actionComposers_from_XML;
    QAction *actionto_XML;
    QAction *actionAlbums_from_XML;
    QAction *actionAlbums_to_XML;
    QWidget *centralWidget;
    QLabel *selectedComposerLabel;
    QWidget *verticalLayoutWidget_2;
    QVBoxLayout *verticalLayout_2;
    QLabel *fullnameLabel;
    QLabel *yearLabel;
    QLabel *amountLabel;
    QLabel *album_label;
    QWidget *horizontalLayoutWidget;
    QHBoxLayout *horizontalLayout;
    QLabel *username_label;
    QLabel *usernameOut_label;
    QLabel *composersLabel;
    QWidget *gridLayoutWidget;
    QGridLayout *gridLayout;
    QHBoxLayout *horizontalLayout_6;
    QPushButton *previousButton;
    QSpinBox *currentPage_spinBox;
    QLabel *allPages_label;
    QPushButton *nextButton;
    QHBoxLayout *horizontalLayout_3;
    QLineEdit *search_lineEdit;
    QListWidget *listWidget;
    QVBoxLayout *verticalLayout_3;
    QPushButton *addButton;
    QPushButton *editButton;
    QPushButton *removeButton;
    QHBoxLayout *horizontalLayout_7;
    QLabel *label;
    QWidget *verticalLayoutWidget;
    QVBoxLayout *verticalLayout;
    QLabel *fullname_labelOut;
    QLabel *year_labelOut;
    QLabel *amount_labelOut;
    QLabel *albumOut_label;
    QLabel *image_label;
    QLabel *qr_label;
    QLabel *information_label;
    QMenuBar *menuBar;
    QMenu *menuFile;
    QMenu *menuImport;
    QMenu *menuExport;
    QMenu *menuUser;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(680, 712);
        QPalette palette;
        QBrush brush(QColor(255, 255, 255, 255));
        brush.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Base, brush);
        QBrush brush1(QColor(238, 238, 236, 255));
        brush1.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Window, brush1);
        palette.setBrush(QPalette::Inactive, QPalette::Base, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Window, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::Base, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::Window, brush1);
        MainWindow->setPalette(palette);
        MainWindow->setCursor(QCursor(Qt::ArrowCursor));
        MainWindow->setFocusPolicy(Qt::WheelFocus);
        MainWindow->setStyleSheet(QStringLiteral("font: 11pt \"URW Bookman L\";"));
        actionNew_storage = new QAction(MainWindow);
        actionNew_storage->setObjectName(QStringLiteral("actionNew_storage"));
        actionOpen_Storage = new QAction(MainWindow);
        actionOpen_Storage->setObjectName(QStringLiteral("actionOpen_Storage"));
        actionExit = new QAction(MainWindow);
        actionExit->setObjectName(QStringLiteral("actionExit"));
        actionSign_up = new QAction(MainWindow);
        actionSign_up->setObjectName(QStringLiteral("actionSign_up"));
        actionCreate_an_account = new QAction(MainWindow);
        actionCreate_an_account->setObjectName(QStringLiteral("actionCreate_an_account"));
        actionLog_in = new QAction(MainWindow);
        actionLog_in->setObjectName(QStringLiteral("actionLog_in"));
        actionLog_out = new QAction(MainWindow);
        actionLog_out->setObjectName(QStringLiteral("actionLog_out"));
        actionComposers_from_XML = new QAction(MainWindow);
        actionComposers_from_XML->setObjectName(QStringLiteral("actionComposers_from_XML"));
        actionto_XML = new QAction(MainWindow);
        actionto_XML->setObjectName(QStringLiteral("actionto_XML"));
        actionAlbums_from_XML = new QAction(MainWindow);
        actionAlbums_from_XML->setObjectName(QStringLiteral("actionAlbums_from_XML"));
        actionAlbums_to_XML = new QAction(MainWindow);
        actionAlbums_to_XML->setObjectName(QStringLiteral("actionAlbums_to_XML"));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        selectedComposerLabel = new QLabel(centralWidget);
        selectedComposerLabel->setObjectName(QStringLiteral("selectedComposerLabel"));
        selectedComposerLabel->setGeometry(QRect(10, 380, 281, 31));
        verticalLayoutWidget_2 = new QWidget(centralWidget);
        verticalLayoutWidget_2->setObjectName(QStringLiteral("verticalLayoutWidget_2"));
        verticalLayoutWidget_2->setGeometry(QRect(250, 420, 171, 201));
        verticalLayout_2 = new QVBoxLayout(verticalLayoutWidget_2);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        verticalLayout_2->setContentsMargins(0, 0, 0, 0);
        fullnameLabel = new QLabel(verticalLayoutWidget_2);
        fullnameLabel->setObjectName(QStringLiteral("fullnameLabel"));

        verticalLayout_2->addWidget(fullnameLabel);

        yearLabel = new QLabel(verticalLayoutWidget_2);
        yearLabel->setObjectName(QStringLiteral("yearLabel"));

        verticalLayout_2->addWidget(yearLabel);

        amountLabel = new QLabel(verticalLayoutWidget_2);
        amountLabel->setObjectName(QStringLiteral("amountLabel"));

        verticalLayout_2->addWidget(amountLabel);

        album_label = new QLabel(verticalLayoutWidget_2);
        album_label->setObjectName(QStringLiteral("album_label"));

        verticalLayout_2->addWidget(album_label);

        horizontalLayoutWidget = new QWidget(centralWidget);
        horizontalLayoutWidget->setObjectName(QStringLiteral("horizontalLayoutWidget"));
        horizontalLayoutWidget->setGeometry(QRect(10, 0, 261, 41));
        horizontalLayout = new QHBoxLayout(horizontalLayoutWidget);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        username_label = new QLabel(horizontalLayoutWidget);
        username_label->setObjectName(QStringLiteral("username_label"));

        horizontalLayout->addWidget(username_label);

        usernameOut_label = new QLabel(horizontalLayoutWidget);
        usernameOut_label->setObjectName(QStringLiteral("usernameOut_label"));

        horizontalLayout->addWidget(usernameOut_label);

        composersLabel = new QLabel(centralWidget);
        composersLabel->setObjectName(QStringLiteral("composersLabel"));
        composersLabel->setGeometry(QRect(10, 40, 329, 22));
        gridLayoutWidget = new QWidget(centralWidget);
        gridLayoutWidget->setObjectName(QStringLiteral("gridLayoutWidget"));
        gridLayoutWidget->setGeometry(QRect(10, 70, 371, 281));
        gridLayout = new QGridLayout(gridLayoutWidget);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        gridLayout->setContentsMargins(0, 0, 0, 0);
        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setSpacing(6);
        horizontalLayout_6->setObjectName(QStringLiteral("horizontalLayout_6"));
        previousButton = new QPushButton(gridLayoutWidget);
        previousButton->setObjectName(QStringLiteral("previousButton"));
        previousButton->setStyleSheet(QStringLiteral("font: italic 11pt \"Ubuntu Mono\";"));

        horizontalLayout_6->addWidget(previousButton);

        currentPage_spinBox = new QSpinBox(gridLayoutWidget);
        currentPage_spinBox->setObjectName(QStringLiteral("currentPage_spinBox"));
        currentPage_spinBox->setCursor(QCursor(Qt::ArrowCursor));
        currentPage_spinBox->setMouseTracking(false);
        currentPage_spinBox->setTabletTracking(false);
        currentPage_spinBox->setFocusPolicy(Qt::NoFocus);
        currentPage_spinBox->setContextMenuPolicy(Qt::NoContextMenu);
        currentPage_spinBox->setAutoFillBackground(false);
        currentPage_spinBox->setWrapping(false);
        currentPage_spinBox->setButtonSymbols(QAbstractSpinBox::NoButtons);
        currentPage_spinBox->setMinimum(1);
        currentPage_spinBox->setMaximum(1000);
        currentPage_spinBox->setValue(1);

        horizontalLayout_6->addWidget(currentPage_spinBox);

        allPages_label = new QLabel(gridLayoutWidget);
        allPages_label->setObjectName(QStringLiteral("allPages_label"));

        horizontalLayout_6->addWidget(allPages_label);

        nextButton = new QPushButton(gridLayoutWidget);
        nextButton->setObjectName(QStringLiteral("nextButton"));
        nextButton->setStyleSheet(QStringLiteral("font: italic 11pt \"Ubuntu Mono\";"));
        nextButton->setCheckable(false);

        horizontalLayout_6->addWidget(nextButton);


        gridLayout->addLayout(horizontalLayout_6, 3, 0, 1, 1);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        search_lineEdit = new QLineEdit(gridLayoutWidget);
        search_lineEdit->setObjectName(QStringLiteral("search_lineEdit"));

        horizontalLayout_3->addWidget(search_lineEdit);


        gridLayout->addLayout(horizontalLayout_3, 0, 0, 1, 1);

        listWidget = new QListWidget(gridLayoutWidget);
        listWidget->setObjectName(QStringLiteral("listWidget"));
        listWidget->setEnabled(true);
        QPalette palette1;
        listWidget->setPalette(palette1);
        listWidget->setFrameShape(QFrame::StyledPanel);
        listWidget->setSelectionMode(QAbstractItemView::MultiSelection);

        gridLayout->addWidget(listWidget, 4, 0, 1, 1);

        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setSpacing(6);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        addButton = new QPushButton(gridLayoutWidget);
        addButton->setObjectName(QStringLiteral("addButton"));

        verticalLayout_3->addWidget(addButton);

        editButton = new QPushButton(gridLayoutWidget);
        editButton->setObjectName(QStringLiteral("editButton"));

        verticalLayout_3->addWidget(editButton);

        removeButton = new QPushButton(gridLayoutWidget);
        removeButton->setObjectName(QStringLiteral("removeButton"));

        verticalLayout_3->addWidget(removeButton);


        gridLayout->addLayout(verticalLayout_3, 4, 1, 1, 1);

        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setSpacing(6);
        horizontalLayout_7->setObjectName(QStringLiteral("horizontalLayout_7"));

        gridLayout->addLayout(horizontalLayout_7, 2, 0, 1, 1);

        label = new QLabel(gridLayoutWidget);
        label->setObjectName(QStringLiteral("label"));

        gridLayout->addWidget(label, 1, 0, 1, 1);

        verticalLayoutWidget = new QWidget(centralWidget);
        verticalLayoutWidget->setObjectName(QStringLiteral("verticalLayoutWidget"));
        verticalLayoutWidget->setGeometry(QRect(420, 420, 251, 201));
        verticalLayout = new QVBoxLayout(verticalLayoutWidget);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        fullname_labelOut = new QLabel(verticalLayoutWidget);
        fullname_labelOut->setObjectName(QStringLiteral("fullname_labelOut"));

        verticalLayout->addWidget(fullname_labelOut);

        year_labelOut = new QLabel(verticalLayoutWidget);
        year_labelOut->setObjectName(QStringLiteral("year_labelOut"));

        verticalLayout->addWidget(year_labelOut);

        amount_labelOut = new QLabel(verticalLayoutWidget);
        amount_labelOut->setObjectName(QStringLiteral("amount_labelOut"));

        verticalLayout->addWidget(amount_labelOut);

        albumOut_label = new QLabel(verticalLayoutWidget);
        albumOut_label->setObjectName(QStringLiteral("albumOut_label"));
        albumOut_label->setEnabled(true);
        QPalette palette2;
        palette2.setBrush(QPalette::Active, QPalette::Base, brush);
        palette2.setBrush(QPalette::Active, QPalette::Window, brush);
        palette2.setBrush(QPalette::Active, QPalette::AlternateBase, brush1);
        palette2.setBrush(QPalette::Inactive, QPalette::Base, brush);
        palette2.setBrush(QPalette::Inactive, QPalette::Window, brush);
        palette2.setBrush(QPalette::Inactive, QPalette::AlternateBase, brush1);
        palette2.setBrush(QPalette::Disabled, QPalette::Base, brush);
        palette2.setBrush(QPalette::Disabled, QPalette::Window, brush);
        palette2.setBrush(QPalette::Disabled, QPalette::AlternateBase, brush1);
        albumOut_label->setPalette(palette2);

        verticalLayout->addWidget(albumOut_label);

        image_label = new QLabel(centralWidget);
        image_label->setObjectName(QStringLiteral("image_label"));
        image_label->setGeometry(QRect(10, 420, 221, 201));
        qr_label = new QLabel(centralWidget);
        qr_label->setObjectName(QStringLiteral("qr_label"));
        qr_label->setGeometry(QRect(430, 140, 191, 181));
        information_label = new QLabel(centralWidget);
        information_label->setObjectName(QStringLiteral("information_label"));
        information_label->setGeometry(QRect(430, 80, 221, 31));
        information_label->setStyleSheet(QStringLiteral("font: 25 11pt \"Umpush\";"));
        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 680, 27));
        menuFile = new QMenu(menuBar);
        menuFile->setObjectName(QStringLiteral("menuFile"));
        menuImport = new QMenu(menuFile);
        menuImport->setObjectName(QStringLiteral("menuImport"));
        menuExport = new QMenu(menuFile);
        menuExport->setObjectName(QStringLiteral("menuExport"));
        menuUser = new QMenu(menuBar);
        menuUser->setObjectName(QStringLiteral("menuUser"));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);

        menuBar->addAction(menuFile->menuAction());
        menuBar->addAction(menuUser->menuAction());
        menuFile->addAction(actionOpen_Storage);
        menuFile->addSeparator();
        menuFile->addAction(menuImport->menuAction());
        menuFile->addAction(menuExport->menuAction());
        menuFile->addSeparator();
        menuFile->addAction(actionExit);
        menuImport->addAction(actionComposers_from_XML);
        menuImport->addAction(actionAlbums_from_XML);
        menuExport->addAction(actionto_XML);
        menuExport->addAction(actionAlbums_to_XML);
        menuUser->addSeparator();
        menuUser->addAction(actionCreate_an_account);
        menuUser->addSeparator();
        menuUser->addAction(actionLog_in);
        menuUser->addAction(actionLog_out);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", Q_NULLPTR));
        actionNew_storage->setText(QApplication::translate("MainWindow", "New storage...", Q_NULLPTR));
#ifndef QT_NO_SHORTCUT
        actionNew_storage->setShortcut(QApplication::translate("MainWindow", "Ctrl+N", Q_NULLPTR));
#endif // QT_NO_SHORTCUT
        actionOpen_Storage->setText(QApplication::translate("MainWindow", "Open Storage...", Q_NULLPTR));
#ifndef QT_NO_SHORTCUT
        actionOpen_Storage->setShortcut(QApplication::translate("MainWindow", "Ctrl+O", Q_NULLPTR));
#endif // QT_NO_SHORTCUT
        actionExit->setText(QApplication::translate("MainWindow", "Exit", Q_NULLPTR));
#ifndef QT_NO_SHORTCUT
        actionExit->setShortcut(QApplication::translate("MainWindow", "Ctrl+W", Q_NULLPTR));
#endif // QT_NO_SHORTCUT
        actionSign_up->setText(QApplication::translate("MainWindow", "Sign up", Q_NULLPTR));
        actionCreate_an_account->setText(QApplication::translate("MainWindow", "Create an account", Q_NULLPTR));
        actionLog_in->setText(QApplication::translate("MainWindow", "Log in", Q_NULLPTR));
        actionLog_out->setText(QApplication::translate("MainWindow", "Log out", Q_NULLPTR));
        actionComposers_from_XML->setText(QApplication::translate("MainWindow", "Composers from XML...", Q_NULLPTR));
        actionto_XML->setText(QApplication::translate("MainWindow", "Composers to XML...", Q_NULLPTR));
        actionAlbums_from_XML->setText(QApplication::translate("MainWindow", "Albums from XML...", Q_NULLPTR));
        actionAlbums_to_XML->setText(QApplication::translate("MainWindow", "Albums to XML...", Q_NULLPTR));
        selectedComposerLabel->setText(QApplication::translate("MainWindow", "Selected composer:", Q_NULLPTR));
        fullnameLabel->setText(QApplication::translate("MainWindow", "Fullname:    ", Q_NULLPTR));
        yearLabel->setText(QApplication::translate("MainWindow", "Year  of birth: ", Q_NULLPTR));
        amountLabel->setText(QApplication::translate("MainWindow", "Amount:", Q_NULLPTR));
        album_label->setText(QApplication::translate("MainWindow", "Albums:", Q_NULLPTR));
        username_label->setText(QApplication::translate("MainWindow", "Username:", Q_NULLPTR));
        usernameOut_label->setText(QString());
        composersLabel->setText(QApplication::translate("MainWindow", "Composers:", Q_NULLPTR));
        previousButton->setText(QApplication::translate("MainWindow", "Previous", Q_NULLPTR));
        currentPage_spinBox->setSpecialValueText(QString());
        allPages_label->setText(QString());
        nextButton->setText(QApplication::translate("MainWindow", "Next", Q_NULLPTR));
        search_lineEdit->setPlaceholderText(QApplication::translate("MainWindow", "Search...", Q_NULLPTR));
        addButton->setText(QApplication::translate("MainWindow", "Add", Q_NULLPTR));
        editButton->setText(QApplication::translate("MainWindow", "Edit", Q_NULLPTR));
        removeButton->setText(QApplication::translate("MainWindow", "Remove", Q_NULLPTR));
        label->setText(QString());
        fullname_labelOut->setText(QString());
        year_labelOut->setText(QString());
        amount_labelOut->setText(QString());
        albumOut_label->setText(QString());
        image_label->setText(QString());
        qr_label->setText(QString());
        information_label->setText(QApplication::translate("MainWindow", "To get more information:", Q_NULLPTR));
        menuFile->setTitle(QApplication::translate("MainWindow", "File", Q_NULLPTR));
        menuImport->setTitle(QApplication::translate("MainWindow", "Import", Q_NULLPTR));
        menuExport->setTitle(QApplication::translate("MainWindow", "Export", Q_NULLPTR));
        menuUser->setTitle(QApplication::translate("MainWindow", "User", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
