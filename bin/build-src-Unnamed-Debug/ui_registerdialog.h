/********************************************************************************
** Form generated from reading UI file 'registerdialog.ui'
**
** Created by: Qt User Interface Compiler version 5.9.5
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_REGISTERDIALOG_H
#define UI_REGISTERDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_RegisterDialog
{
public:
    QDialogButtonBox *buttonBox;
    QLabel *message_label;
    QWidget *gridLayoutWidget;
    QGridLayout *gridLayout;
    QLabel *username_label;
    QLabel *passwordAgain_label;
    QLabel *password_label;
    QLineEdit *username_lineEdit;
    QLineEdit *password_lineEdit;
    QLineEdit *passwordAgain_lineEdit;
    QLabel *warning_label;

    void setupUi(QDialog *RegisterDialog)
    {
        if (RegisterDialog->objectName().isEmpty())
            RegisterDialog->setObjectName(QStringLiteral("RegisterDialog"));
        RegisterDialog->resize(596, 300);
        buttonBox = new QDialogButtonBox(RegisterDialog);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setGeometry(QRect(30, 240, 341, 32));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);
        message_label = new QLabel(RegisterDialog);
        message_label->setObjectName(QStringLiteral("message_label"));
        message_label->setGeometry(QRect(20, 20, 301, 21));
        gridLayoutWidget = new QWidget(RegisterDialog);
        gridLayoutWidget->setObjectName(QStringLiteral("gridLayoutWidget"));
        gridLayoutWidget->setGeometry(QRect(20, 70, 351, 141));
        gridLayout = new QGridLayout(gridLayoutWidget);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        gridLayout->setContentsMargins(0, 0, 0, 0);
        username_label = new QLabel(gridLayoutWidget);
        username_label->setObjectName(QStringLiteral("username_label"));

        gridLayout->addWidget(username_label, 0, 0, 1, 1);

        passwordAgain_label = new QLabel(gridLayoutWidget);
        passwordAgain_label->setObjectName(QStringLiteral("passwordAgain_label"));

        gridLayout->addWidget(passwordAgain_label, 2, 0, 1, 1);

        password_label = new QLabel(gridLayoutWidget);
        password_label->setObjectName(QStringLiteral("password_label"));

        gridLayout->addWidget(password_label, 1, 0, 1, 1);

        username_lineEdit = new QLineEdit(gridLayoutWidget);
        username_lineEdit->setObjectName(QStringLiteral("username_lineEdit"));

        gridLayout->addWidget(username_lineEdit, 0, 1, 1, 1);

        password_lineEdit = new QLineEdit(gridLayoutWidget);
        password_lineEdit->setObjectName(QStringLiteral("password_lineEdit"));
        password_lineEdit->setEchoMode(QLineEdit::Password);

        gridLayout->addWidget(password_lineEdit, 1, 1, 1, 1);

        passwordAgain_lineEdit = new QLineEdit(gridLayoutWidget);
        passwordAgain_lineEdit->setObjectName(QStringLiteral("passwordAgain_lineEdit"));
        passwordAgain_lineEdit->setEchoMode(QLineEdit::Password);

        gridLayout->addWidget(passwordAgain_lineEdit, 2, 1, 1, 1);

        warning_label = new QLabel(RegisterDialog);
        warning_label->setObjectName(QStringLiteral("warning_label"));
        warning_label->setGeometry(QRect(380, 130, 211, 31));
        QPalette palette;
        QBrush brush(QColor(204, 0, 0, 255));
        brush.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::WindowText, brush);
        palette.setBrush(QPalette::Active, QPalette::Text, brush);
        palette.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Text, brush);
        QBrush brush1(QColor(190, 190, 190, 255));
        brush1.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Disabled, QPalette::WindowText, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::Text, brush1);
        warning_label->setPalette(palette);
        warning_label->setTextFormat(Qt::PlainText);

        retranslateUi(RegisterDialog);
        QObject::connect(buttonBox, SIGNAL(accepted()), RegisterDialog, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), RegisterDialog, SLOT(reject()));

        QMetaObject::connectSlotsByName(RegisterDialog);
    } // setupUi

    void retranslateUi(QDialog *RegisterDialog)
    {
        RegisterDialog->setWindowTitle(QApplication::translate("RegisterDialog", "Dialog", Q_NULLPTR));
        message_label->setText(QApplication::translate("RegisterDialog", "Enter your data:", Q_NULLPTR));
        username_label->setText(QApplication::translate("RegisterDialog", "Username:", Q_NULLPTR));
        passwordAgain_label->setText(QApplication::translate("RegisterDialog", "Password again:", Q_NULLPTR));
        password_label->setText(QApplication::translate("RegisterDialog", "Password:", Q_NULLPTR));
        warning_label->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class RegisterDialog: public Ui_RegisterDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_REGISTERDIALOG_H
