/********************************************************************************
** Form generated from reading UI file 'authdialog.ui'
**
** Created by: Qt User Interface Compiler version 5.9.5
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_AUTHDIALOG_H
#define UI_AUTHDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_AuthDialog
{
public:
    QDialogButtonBox *buttonBox;
    QLabel *userAuth_label;
    QWidget *gridLayoutWidget;
    QGridLayout *gridLayout;
    QLabel *username_label;
    QLineEdit *username_lineEdit;
    QLabel *password_label;
    QLineEdit *password_lineEdit;
    QPushButton *pushButton;

    void setupUi(QDialog *AuthDialog)
    {
        if (AuthDialog->objectName().isEmpty())
            AuthDialog->setObjectName(QStringLiteral("AuthDialog"));
        AuthDialog->resize(404, 337);
        AuthDialog->setStyleSheet(QStringLiteral("font: 11pt \"URW Bookman L\";"));
        buttonBox = new QDialogButtonBox(AuthDialog);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setGeometry(QRect(50, 290, 341, 32));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);
        userAuth_label = new QLabel(AuthDialog);
        userAuth_label->setObjectName(QStringLiteral("userAuth_label"));
        userAuth_label->setGeometry(QRect(10, 10, 251, 31));
        gridLayoutWidget = new QWidget(AuthDialog);
        gridLayoutWidget->setObjectName(QStringLiteral("gridLayoutWidget"));
        gridLayoutWidget->setGeometry(QRect(0, 50, 381, 131));
        gridLayout = new QGridLayout(gridLayoutWidget);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        gridLayout->setContentsMargins(0, 0, 0, 0);
        username_label = new QLabel(gridLayoutWidget);
        username_label->setObjectName(QStringLiteral("username_label"));

        gridLayout->addWidget(username_label, 0, 0, 1, 1);

        username_lineEdit = new QLineEdit(gridLayoutWidget);
        username_lineEdit->setObjectName(QStringLiteral("username_lineEdit"));

        gridLayout->addWidget(username_lineEdit, 0, 1, 1, 1);

        password_label = new QLabel(gridLayoutWidget);
        password_label->setObjectName(QStringLiteral("password_label"));

        gridLayout->addWidget(password_label, 2, 0, 1, 1);

        password_lineEdit = new QLineEdit(gridLayoutWidget);
        password_lineEdit->setObjectName(QStringLiteral("password_lineEdit"));
        password_lineEdit->setEchoMode(QLineEdit::Password);

        gridLayout->addWidget(password_lineEdit, 2, 1, 1, 1);

        pushButton = new QPushButton(AuthDialog);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(160, 190, 221, 30));

        retranslateUi(AuthDialog);
        QObject::connect(buttonBox, SIGNAL(accepted()), AuthDialog, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), AuthDialog, SLOT(reject()));

        QMetaObject::connectSlotsByName(AuthDialog);
    } // setupUi

    void retranslateUi(QDialog *AuthDialog)
    {
        AuthDialog->setWindowTitle(QApplication::translate("AuthDialog", "Dialog", Q_NULLPTR));
        userAuth_label->setText(QApplication::translate("AuthDialog", "TextLabel", Q_NULLPTR));
        username_label->setText(QApplication::translate("AuthDialog", "Username", Q_NULLPTR));
        username_lineEdit->setPlaceholderText(QApplication::translate("AuthDialog", "Tyler Joseph", Q_NULLPTR));
        password_label->setText(QApplication::translate("AuthDialog", "Password", Q_NULLPTR));
        password_lineEdit->setPlaceholderText(QApplication::translate("AuthDialog", "very strong password", Q_NULLPTR));
        pushButton->setText(QApplication::translate("AuthDialog", "Don`t have an account?", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class AuthDialog: public Ui_AuthDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_AUTHDIALOG_H
